/*
 * CandyPacker.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class is responsible for creating threads for packing candies and
 * storing them in a shared storage.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class CandyPacker extends Thread {
	static Object storageLock = new Object();
	static volatile int boxesCount;
	CandyWrapper candyWrapper;
	BoxMaker boxProducer;
	int delay = 1200;

	// maximum number of boxes to pack
	static volatile int MAX_BOXES;

	/*
	 * Constructor for initializing candy maker threads. It take references of
	 * the Candy wrapper and box makers
	 */
	public CandyPacker(CandyWrapper candies, BoxMaker box, int boxCount) {
		boxesCount = 0;
		candyWrapper = candies;
		boxProducer = box;
		MAX_BOXES = boxCount;
	}

	/*
	 * This method is responsible for continuously packing candies with a
	 * predefined delay in process, until the required number of boxes are
	 * packed. Each box is stored in a shared storage only after acquiring a
	 * shared common storage lock between threads.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void run() {
		while (boxesCount < MAX_BOXES)
			packCandies();
		System.out.println("Completed Task : packed " + boxesCount + " boxes");
		System.out.println("CandyPacker Stopped");
	}

	/*
	 * This method contains the main candy packing process. The process take a
	 * pre-defined delay in the candy packing. Each new box is stored in a
	 * shared storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void packCandies() {

		// get wrapped candies
		candyWrapper.getWrappedCandies();
		// get boxes for packing
		boxProducer.getBox();
		System.out.println("Packer : PACKING new box");
		try {
			sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("Packer : Adding new PACKED box to storage");

		// add new packed box to storage
		addBoxToStorage();
	}

	/*
	 * This method acts as a synchronized shared storage for all candy packing
	 * threads. Each thread must aquire lock to add new box to the storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void addBoxToStorage() {
		synchronized (storageLock) {
			boxesCount++;
		}
	}
}
