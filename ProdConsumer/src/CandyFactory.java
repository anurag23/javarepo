/*
 * CandyFactory.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class acts as a candy producing factory with different production units
 * including candy makers, wrapping paper maker, box maker, candy wrapper and
 * the final candy packing unit. Each of these units are implemented as
 * independent units using multiple threads, handled only by the main Candy
 * Factory thread.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class CandyFactory {

	/*
	 * Program execution starts from the main method, which is responsible for
	 * creating multiple threads of various other independent producer and
	 * consumer threads. All these threads are allowed to work independently
	 * until the required number of candy boxes are packed. The number of boxes
	 * to be packed are provided as command line parameter.
	 * 
	 * params : String[] - command line parameters
	 */
	public static void main(String[] args) {
		// create candy maker thread
		CandyMaker candyMaker = new CandyMaker();

		// create wrapping paper maker thread
		PaperMaker paperMaker = new PaperMaker();

		// create box maker thread
		BoxMaker boxMaker = new BoxMaker();

		// create candy wrapping thread, this thread needs reference of the
		// candy and paper producing threads.
		CandyWrapper candyWrapper = new CandyWrapper(candyMaker, paperMaker);

		// number of candy boxes to be packed
		int boxCount = Integer.parseInt(args[0]);

		// Candy packer thread, this thread communicates with the candy wrapping
		// and box producing threads.
		CandyPacker candyPacker = new CandyPacker(candyWrapper, boxMaker,
				boxCount);

		// start all the threads
		candyMaker.start();
		paperMaker.start();
		boxMaker.start();
		candyWrapper.start();
		candyPacker.start();

		// wait for the candy packing thread to complete its task
		try {
			candyPacker.join();
		} catch (InterruptedException e) {
		}

		// stop all other threads
		candyMaker.stopProcess();
		paperMaker.stopProcess();
		boxMaker.stopProcess();
		candyWrapper.stopProcess();
	} // main

} // CandyFactory
