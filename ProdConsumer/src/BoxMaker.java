/*
 * BoxMaker.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class is responsible for creating threads producing wrapping boxes and
 * storing them in a shared storage.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class BoxMaker extends Thread {
	// shared lock for storage
	static Object storageLock = new Object();
	// shared storage
	static int boxesCount;
	// stop flag
	volatile boolean stop;
	// maximum storage size
	int maxStorageSize = 20;
	// delay in production of each next box
	int delay = 1000;

	/*
	 * Constructor for initializing box maker threads
	 */
	public BoxMaker() {
		// boxesCount = 0;
		stop = false;
	}

	/*
	 * This method is responsible for continuously producing boxes with a
	 * predefined delay in process, until it a stop flag is turned on. Each
	 * box is stored in a shared storage only after acquiring a shared common
	 * storage lock between threads.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void run() {
		// run continuosly until stop flag is turned on
			while (!stop)
				makeBox();
			System.out.println("BoxMaker Stopped");
	}

	/*
	 * This method is used by the master thread to set a stop flag and indicate
	 * the box making threads to stop processing new boxes. Any thread in
	 * storage is allowed to store the last box and then come to halt.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void stopProcess() {
		// set stop flag true
		stop = true;
		// interrupt the current thread
		this.interrupt();
	}

	/*
	 * This method contains the main box making process. The process take a
	 * pre-defined delay in the box production. Each new box is stored in a
	 * shared storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void makeBox() {

		System.out.println("BoxMaker : making new BOX");
		try {
			sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("BoxMaker : Adding BOX to storage");

		// if stop flag is not raised, add the new box to storage
		if(!stop) addBoxToStorage();
	}

	/*
	 * This method acts as a synchronized shared storage for all box producing
	 * threads. Each thread must aquire lock to add new box to the storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void addBoxToStorage() {
		
		// aquire storage lock or wait
		synchronized (storageLock) {
			// if the storage is full, wait for a consumer to notify after
			// consumption
			if (boxesCount > maxStorageSize) {
				try {
					System.out.println("Box storage full : Waiting");
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			// add box to storage, update count
			boxesCount++;
			// notify all waiting consumer threads
			storageLock.notifyAll();
		}
	}

	/*
	 * This method acts as a interface for the consumers to get boxes from a
	 * shared storage. All consumer threads must aquire a shared storage lock
	 * before getting boxes.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void getBox() {
		
		// aquire storage lock
		synchronized (storageLock) {
			
			// if storage is empty, wait for a producer thread to notify
			if (boxesCount < 1) {
				try {
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			// get boxes and update count
			boxesCount--;
			// notify all waiting producer threads
			storageLock.notifyAll();
		}
	}
}