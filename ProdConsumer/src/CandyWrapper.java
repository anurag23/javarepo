/*
 * CandyWrapper.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class is responsible for creating threads for wrappeing candies and
 * storing them in a shared storage.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class CandyWrapper extends Thread {
	static Object storageLock = new Object();
	static int wrappedCandies;
	CandyMaker candyProducer;
	PaperMaker paperProducer;
	volatile boolean stop;
	int maxStorageSize = 20;
	int delay = 1000;

	/*
	 * Constructor for initializing candy wrapping threads. It take references of
	 * the Candy and Paper makers
	 */
	public CandyWrapper(CandyMaker candy, PaperMaker paper) {
		wrappedCandies = 0;
		candyProducer = candy;
		paperProducer = paper;
	}

	/*
	 * This method is responsible for continuously wrapping candies with a
	 * predefined delay in process, until it a stop flag is turned on. Each
	 * candy is stored in a shared storage only after acquiring a shared common
	 * storage lock between threads.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void run() {
		// run continuosly until stop flag is turned on
		while (!stop)
			// make next candy
			wrapCandy();
		System.out.println("CandyWrapper Stopped");
	}

	/*
	 * This method is used by the master thread to set a stop flag and indicate
	 * the candy wrapping threads to stop processing new candies. Any thread in
	 * storage is allowed to store the last candy and then come to halt.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void stopProcess() {
		stop = true;
		this.interrupt();
	}

	/*
	 * This method contains the main candy wrapping process. The process take a
	 * pre-defined delay in the candy wrapping. Each new candy is stored in a
	 * shared storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void wrapCandy() {

		// get candies from the candy producer
		candyProducer.getCandy();
		// get wrapping candies from the candy producer
		paperProducer.getPaper();
		System.out.println("Wrapper : WRAPPING next Candy");
		try {
			sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("Wrapper : Adding new WRAPPED candy to storage");

		// if stop flag is not raised, add the new candy to storage
		if (!stop)
			addCandyToStorage();
	}

	/*
	 * This method acts as a synchronized shared storage for all candy wrapping
	 * threads. Each thread must aquire lock to add new candy to the storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void addCandyToStorage() {
		// aquire storage lock or wait
		synchronized (storageLock) {
			// if the storage is full, wait for a consumer to notify after
			// consumption
			if (wrappedCandies > maxStorageSize) {
				try {
					System.out.println("Wrapped candy storage full : Waiting");
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			wrappedCandies++;
			storageLock.notifyAll();
		}
	}

	/*
	 * This method acts as a interface for the consumers to get candies from a
	 * shared storage. All consumer threads must aquire a shared storage lock
	 * before getting candies.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void getWrappedCandies() {
		// aquire storage lock
		synchronized (storageLock) {
			// if storage is empty, wait for a producer thread to notify
			if (wrappedCandies < 1) {
				try {
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			// get candies and update count
			wrappedCandies -= 4;
			// notify all waiting producer threads
			storageLock.notifyAll();
		}
	}

}
