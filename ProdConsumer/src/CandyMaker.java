/*
 * CandyMaker.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class is responsible for creating threads producing candies and storing
 * them in a shared storage.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class CandyMaker extends Thread {
	// shared lock for storage
	static Object storageLock = new Object();
	// shared storage
	static int candyCount;
	// maximum storage size
	int maxStorageSize = 20;
	// delay in production of each next candy
	int delay = 1000;
	// stop flag
	volatile boolean stop;

	/*
	 * Constructor for initializing candy maker threads
	 */
	public CandyMaker() {
		// candyCount = 0;
		stop = false;
	}

	/*
	 * This method is responsible for continuously producing candies with a
	 * predefined delay in process, until it a stop flag is turned on. Each
	 * candy is stored in a shared storage only after acquiring a shared common
	 * storage lock between threads.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void run() {
		// run continuosly until stop flag is turned on
		while (!stop)
			// make next candy
			makeCandy();
		System.out.println("CandyMaker Stopped");
	}

	/*
	 * This method is used by the master thread to set a stop flag and indicate
	 * the candy making threads to stop processing new candies. Any thread in
	 * storage is allowed to store the last candy and then come to halt.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void stopProcess() {
		// set stop flag true
		stop = true;
		// interrupt the current thread
		this.interrupt();
	}

	/*
	 * This method contains the main candy making process. The process take a
	 * pre-defined delay in the candy production. Each new candy is stored in a
	 * shared storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void makeCandy() {
		// produce new candy
		System.out.println("CandyMaker : making new candy");
		try {
			sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("CandyMaker : Adding new candy to storage");

		// if stop flag is not raised, add the new candy to storage
		if (!stop)
			addCandyToStorage();
	}

	/*
	 * This method acts as a synchronized shared storage for all candy producing
	 * threads. Each thread must aquire lock to add new candy to the storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void addCandyToStorage() {

		// aquire storage lock or wait
		synchronized (storageLock) {

			// if the storage is full, wait for a consumer to notify after
			// consumption
			if (candyCount > maxStorageSize) {
				try {
					System.out.println("Candy storage full : Waiting");
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}

			// add candy to storage, update count
			candyCount++;

			// notify all waiting consumer threads
			storageLock.notifyAll();
		}
	}

	/*
	 * This method acts as a interface for the consumers to get candies from a
	 * shared storage. All consumer threads must aquire a shared storage lock
	 * before getting candies.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void getCandy() {

		// aquire storage lock
		synchronized (storageLock) {

			// if storage is empty, wait for a producer thread to notify
			if (candyCount < 1) {
				try {
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}

			// get candies and update count
			candyCount--;

			// notify all waiting producer threads
			storageLock.notifyAll();
		}
	}
}
