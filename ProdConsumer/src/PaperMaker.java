/*
 * PaperMaker.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class is responsible for creating threads producing wrapping papers and
 * storing them in a shared storage.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class PaperMaker extends Thread {
	// shared lock for storage
	static Object storageLock = new Object();
	// shared storage
	int paperCount;
	// maximum storage size
	int maxStorageSize = 20;
	// delay in production of each next paper
	int delay = 1200;
	// stop flag
	volatile boolean stop;

	/*
	 * Constructor for initializing paper maker threads
	 */
	public PaperMaker() {
		//paperCount = 0;
		stop = false;
	}

	/*
	 * This method is responsible for continuously producing papers with a
	 * predefined delay in process, until it a stop flag is turned on. Each
	 * paper is stored in a shared storage only after acquiring a shared common
	 * storage lock between threads.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void run() {
		// run continuosly until stop flag is turned on
		while (!stop)
			// make next paper
			makePaper();
		System.out.println("PaperMaker Stopped");
	}

	/*
	 * This method is used by the master thread to set a stop flag and indicate
	 * the paper making threads to stop processing new papers. Any thread in
	 * storage is allowed to store the last paper and then come to halt.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void stopProcess() {
		// set stop flag true
		stop = true;
		// interrupt the current thread
		this.interrupt();
	}

	/*
	 * This method contains the main paper making process. The process take a
	 * pre-defined delay in the paper production. Each new paper is stored in a
	 * shared storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void makePaper() {
		// produce new paper
		System.out.println("PaperMaker : making PAPERS");
		try {
			sleep(delay);
		} catch (InterruptedException e) {
		}
		System.out.println("PaperMaker : Adding PAPERS to storage");

		// if stop flag is not raised, add the new paper to storage
		if (!stop)
			addPaperToStorage();
	}

	/*
	 * This method acts as a synchronized shared storage for all paper producing
	 * threads. Each thread must aquire lock to add new paper to the storage.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void addPaperToStorage() {
		
		// aquire storage lock or wait
		synchronized (storageLock) {
			// if the storage is full, wait for a consumer to notify after
						// consumption
			while (paperCount > maxStorageSize - 3) {
				try {
					System.out.println("Paper storage full : Waiting");
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			// add paper to storage, update count
			paperCount += 3;
			// notify all waiting consumer threads
			storageLock.notifyAll();
		}
	}

	/*
	 * This method acts as a interface for the consumers to get papers from a
	 * shared storage. All consumer threads must aquire a shared storage lock
	 * before getting papers.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void getPaper() {
		
		// aquire storage lock
		synchronized (storageLock) {
			
			// if storage is empty, wait for a producer thread to notify
			if (paperCount < 1) {
				try {
					storageLock.wait();
				} catch (InterruptedException e) {
				}
			}
			// get papers and update count
			paperCount--;
			// notify all waiting producer threads
			storageLock.notify();
		}
	}

}
