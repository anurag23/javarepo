import java.util.*;

/*
 * HashSetNew.java
 * 
 * Version : 1
 * 
 * Revision : 1
 * 
 */

/**
 * This class represents a HashSet capable of providing optimised store and
 * retrival options.
 * 
 * @author Anurag Malik
 */
public class HashSetNew extends HashSet<Object> {
	private static final long serialVersionUID = 1L;
	private transient HashMap<Object, Object> map;
	final Object sameVal = new Object();

	// Instantiate a new underlying hashmap
	HashSetNew() {
		map = new HashMap<Object, Object>();
	} // HashMap

	/*
	 * Add method is responsible for adding new values in the hashset.
	 */
	public boolean add(Object key) {
		// put the new entry (Key) with default value object
		// into the underlying hashmap
		return map.put(key, sameVal) != -1;
	}

	/*
	 * This method clears all the values stored in the hashset.
	 */
	public void clear() {
		map.clear();
	}

	/*
	 * This method is responsible for searching the input key value in the
	 * hashset.
	 */
	public boolean contains(Object key) {
		return map.containsKey(key);
	}

	/*
	 * This method is responsible for searching and removing the required input
	 * key value from the hashset.
	 */
	public boolean remove(Object key) {
		return map.remove(key) == sameVal;
	}

	/*
	 * This method returns a iterator instance on all the values stored in the
	 * hashset.
	 */
	public Iterator<Object> iterator() {
		return map.keySet().iterator();
	}

	/*
	 * This method returns the size/ number of elements stored in the hashset
	 */
	public int size() {
		return map.size();
	}

	/*
	 * This method returns true if the hashset is empty and there are no values
	 * stored in the set.
	 */
	public boolean isEmpty() {
		return map.isEmpty();
	}
}
