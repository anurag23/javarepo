import java.util.AbstractSet;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/*
 * HashMap.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

/**
 * Hashmap imlementation capable of storing key value pairs into linked list
 * records. Key - String Value - Integer
 * 
 * @author Anurag Malik, am3926
 */
public class HashMap<K, V> {
	private Record<K, V>[] map;
	private int size;
	private final int MAX_SIZE = 1000;
	// count of modifications made to the hash table
	private transient volatile int modCount;

	@SuppressWarnings("unchecked")
	public HashMap() {
		map = new Record[MAX_SIZE];
		this.size = 0;
	} // HashMap

	@SuppressWarnings("unchecked")
	public HashMap(int size) {
		map = new Record[size];
		this.size = 0;
	} // HashMap

	/*
	 * This method is used to put new key-value pair into the hash map data
	 * structure. A hashing function is used to get a hash value on the input
	 * key. A new record is then saved at the appropriate index in hash table.
	 */
	public int put(K newKey, V value) {
		if (newKey == null || containsKey(newKey))
			return -1;

		// generate hash code for the key
		int hash = hash(newKey);
		Record<K, V> newEntry = new Record<K, V>(newKey, value, null);

		// no collision case, insert new entry at index
		if (map[hash] == null) {
			map[hash] = newEntry;
			modCount++;
			size += 1;
			return hash;
		} else {
			// collision case - add new entry at the end of the bucket list.
			Record<K, V> previous = null;
			Record<K, V> iterator = map[hash];

			while (iterator != null) {
				// key found
				if (iterator.getKey().equals(newKey)) {
					if (previous == null) {
						newEntry.next = iterator.getNext();
						map[hash] = newEntry;
						modCount++;
						size += 1;
						return hash;
					} else {
						newEntry.next = iterator.getNext();
						previous.next = newEntry;
						modCount++;
						size += 1;
						return hash;
					}
				}
				previous = iterator;
				iterator = iterator.getNext();
			}
			// append new entry to the bucket list
			previous.next = newEntry;
			size += 1;
			return hash;
		}
	} // put

	/*
	 * This method returns the size of the hashmap, which is the total number of
	 * elements in the hash map at any given point of time.
	 */
	public int size() {
		return size;
	} // size

	/*
	 * This method returns true if the hashmap is empty and contain no data,
	 * else return false.
	 */
	public boolean isEmpty() {
		return size == 0;
	} // isEmpty

	/*
	 * This method is responsible for first searching an input key in the hash
	 * map and then return back its associated value stored in the map. It
	 * returns null if no matching key is found.
	 */
	public Object get(K key) {
		int index = hash(key);
		if (map[index] == null) {
			return null;
		} else {
			Record<K, V> node = map[index];
			while (node != null) {
				if (node.getKey().equals(key))
					return (Object) node.getValue();
				node = node.next;
			}
			return null;
		}
	} // get

	/*
	 * This method is responsible for clearing all the existing key-value pairs
	 * stored in the hash map.
	 */
	public void clear() {
		modCount++;
		Record<K, V>[] records = map;
		for (int i = 0; i < records.length; i++)
			records[i] = null;
		size = 0;
	} // clear

	/*
	 * Hash method is responsible for generating hashcode for all input key
	 * values.
	 */
	public int hash(Object key) {
		int hash = Math.abs(key.hashCode());
		hash ^= (hash >>> 20) ^ (hash >>> 12);
		return (hash ^ (hash >>> 7) ^ (hash >>> 4)) % MAX_SIZE;
		// return hash % MAX_SIZE;
	} // hash

	/*
	 * This method is used to search if there exist data for an input key in the
	 * Hashmap. It returns TRUE, if a matching key is found in the map else it
	 * returns false.
	 */
	public boolean containsKey(Object key) {
		if (map == null)
			return false;

		// generate hashcode for key
		int index = hash(key);
		// return if no value is found at index
		if (map[index] == null) {
			return false;
		} else {
			// search the bucket list for required key
			Record<K, V> node = map[index];
			while (node != null) {
				if (node.getKey().equals(key))
					// key found
					return true;
				node = node.next;
			}
			return false;
		}
	} // containsKey

	/*
	 * This method is used to retrieve a set of all the keys stored in the
	 * hashmap. this allows other applications to iterate over all the values in
	 * the map.
	 */
	public Set<K> keySet() {
		Set<K> keys = new KeySet();
		return keys;
	} // keySet

	/*
	 * Remove method is used to search for an input key in the hashmap data
	 * structure and remove the matching entry if found.
	 */
	public V remove(Object key) {

		// get hashcode
		int index = hash(key);
		if (map[index] == null) {
			return null;
		} else {
			// search the bucket list for possible matching key
			Record<K, V> node = map[index];
			Record<K, V> prev = null;
			while (node != null) {
				if (node.getKey().equals(key)) {
					// key found, remove this item from the bucket list.
					if (prev != null)
						prev.next = node.next;
					else
						map[index] = node.next;
					node.next = null;
					modCount++;
					size -= 1;
					return node.getValue();
				}
				prev = node;
				node = node.next;
			}
			return null;
		}
	} // remove

	/**
	 * This private class is used by the HashMap class to provide an iteration
	 * machanism over all its key set. It also provides a set of functions like
	 * size, remove and contains method for execution on the key set of hashmap.
	 * 
	 * @author Anurag Malik
	 * 
	 */
	private class KeySet extends AbstractSet<K> {
		public Iterator<K> iterator() {
			// genearate new HashIterator instance
			return new HashIterator<K>() {
				public K next() {
					return nextEntry().getKey();
				}
			};
		}

		/*
		 * Return size of the KeySet
		 */
		public int size() {
			return size;
		} // size

		/*
		 * Check if the required input key is present in the hashmap
		 */
		public boolean contains(Object key) {
			return HashMap.this.containsKey(key);
		} // contains

		/*
		 * This method is execute on the keyset instance to search and remove a
		 * required key from the underlying hashmap data structure.
		 */
		public boolean remove(Object key) {
			return HashMap.this.remove(key) != null;
		} // remove

		/*
		 * This method is responsible for clearing all the values stored in the
		 * underlying hashmap data structure.
		 */
		public void clear() {
			HashMap.this.clear();
		} // clear

	} // KeySet

	/**
	 * This private class provides a fast-fail iteration mechanism on the
	 * hashmap entries. It iterates over the non-null values of the map.
	 * 
	 * @author Anurag Malik
	 */
	private abstract class HashIterator<E> implements Iterator<E> {
		Record<K, V> nextRecord;
		int itrCount;
		int index;
		Record<K, V> current;

		/*
		 * Instanciate new iterator, reset next record to the first non-null
		 * value in map.
		 */
		HashIterator() {
			itrCount = modCount;
			if (size > 0) {
				Record<K, V>[] rec = map;
				// search next value
				while (index < rec.length
						&& (nextRecord = rec[index++]) == null)
					;
			}
		}

		/*
		 * This method returns TRUE if there exist another non-null value in the
		 * hashmap being iterated.
		 */
		public final boolean hasNext() {
			return nextRecord != null;
		}

		/*
		 * This method returns the next element in the underlying data structure
		 * being iterated.
		 */
		final Record<K, V> nextEntry() {
			// check if the underlying hashmap has been modified concurrently
			// from within the iterator, raise exception
			if (modCount != itrCount)
				throw new ConcurrentModificationException();
			Record<K, V> curRec = nextRecord;
			// if there is no other non-null value present, raise exception
			if (curRec == null)
				throw new NoSuchElementException();

			// update next record in this iteration
			if ((nextRecord = curRec.next) == null) {
				Record<K, V>[] t = map;
				while (index < t.length && (nextRecord = t[index++]) == null)
					;
			}
			current = curRec;
			return curRec;
		} // nextEntry

		/*
		 * This method is used to safely remove the current value being
		 * iterated.
		 */
		public void remove() {
			if (current == null)
				throw new IllegalStateException();
			if (modCount != itrCount)
				throw new ConcurrentModificationException();
			K cur = current.getKey();
			current = null;
			// call remove for the key on the underlying hashmap data structure
			HashMap.this.remove(cur);
			itrCount = modCount;
		} // remove
	} // HashIterator

} // HashMap

/**
 * This class represents a generic data item from the HashMap
 * 
 * @author Anurag Malik
 * 
 * @param <T>
 *            : key
 * @param <M>
 *            : value
 */
class Record<T, M> {
	T key;
	M value;
	Record<T, M> next;

	// initialize record
	public Record(T key, M value, Record<T, M> next) {
		this.key = key;
		this.value = value;
		this.next = next;
	}

	// return key
	public T getKey() {
		return key;
	}

	// return value
	public M getValue() {
		return value;
	}

	// return pointer to next record
	public Record<T, M> getNext() {
		return next;
	}
}