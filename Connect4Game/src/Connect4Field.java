/*
 * Connect4Field.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.util.Scanner;

/**
 * This class implements a two player board game, popularly known as the
 * Connect-Four or Four Connect game. The game is based upon the idea of
 * dropping coloured stones/ pieces into a rectangular vertical board with
 * multiple slots. The aim of each player is to create a series of 4 connecting
 * pieces of his/her colour and avaoid another player from doing the same. First
 * one to get 4 connected pieces is the winner.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
 */
public class Connect4Field implements Connect4FieldInterface {

	private char[][] gameBoard;
	private int boardRows, boardColumns, lastMoveRow, lastMoveCol;
	private PlayerInterface[] thePlayers = new PlayerInterface[2];
	private Scanner inputScanner;

	// default constructor - responsible for creating the board.
	// size of the board is triangular with 25 Column and 9 Rows.
	public Connect4Field() {
		super();
		boardRows = 9;
		boardColumns = 25;

		// create the gameboard array of characters
		this.gameBoard = new char[boardRows][boardColumns];

		// fill each character on game board with their default values
		for (int rowNo = 0; rowNo < boardRows; rowNo++) {
			for (int colNo = 0; colNo < boardColumns; colNo++)
				if (colNo >= rowNo && colNo < boardColumns - rowNo)
					gameBoard[rowNo][colNo] = '0';
				else
					// edges are set blank
					gameBoard[rowNo][colNo] = ' ';
			inputScanner = new Scanner(System.in);
		}

	} // Connect4Field

	/*
	 * parameterized constructor - responsible for calling default constructor
	 * and also creating the game players
	 */
	public Connect4Field(Boolean play) {

		// calling default constructor
		this();

		// initialize players for the game - Player/Computer or Player/Player
		init(thePlayers[0], thePlayers[1]);

	} // Connect4Field

	@Override
	/*
	 * Check if a piece can be dropped in the input column. It returns FALSE -
	 * if the top row in input column is not already filled with another piece
	 * 
	 * @params - int - column number in which the piece has to be dropped
	 * 
	 * @return - boolean - true if the top row in input column is empty
	 * 
	 * @see Connect4FieldInterface#checkIfPiecedCanBeDroppedIn(int)
	 */
	public boolean checkIfPiecedCanBeDroppedIn(int column) {

		// return true of the column number lies within board limits and
		// the top row for this column is not already filled with another piece
		if (column > 0 && column <= boardColumns
				&& gameBoard[0][column - 1] == '0')
			return true;
		else
			return false;
	} // checkIfPiecedCanBeDroppedIn

	@Override
	/*
	 * This method places the next input piece on top of all other pieces in the
	 * selected column, If the move is not allowed, it prints an error message
	 * (Javadoc)
	 * 
	 * @params - column number, game piece
	 * 
	 * @return - void
	 * 
	 * @see Connect4FieldInterface#dropPieces(int, char)
	 */
	public void dropPieces(int column, char gamePiece) {

		// check if a piece can be dropped in selected column
		if (checkIfPiecedCanBeDroppedIn(column)) {
			int row = 0;

			// find the top row which is empty in current column
			while (row < boardRows && gameBoard[row][column - 1] == '0') {
				row++;
			}
			// set the location as game piece
			gameBoard[--row][column - 1] = gamePiece;

			// save row and column number of last move
			lastMoveRow = row;
			lastMoveCol = column - 1;
		} else {

			// display error message
			System.out.println("Invalid Turn");
		}
	} // dropPieces

	/*
	 * This method is responsible for reverting back the last move and replace
	 * the last piece location with 0.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void undoLastMove() {

		// revert back the last move
		gameBoard[lastMoveRow][lastMoveCol] = '0';
		lastMoveRow = 0;
		lastMoveCol = 0;
	} // undoLastMove

	@Override
	/*
	 * This method check if the last move resulted in a win. The following
	 * evaluation process is used to check for a winning condition : - check if
	 * there exist a group of four similar pieces same as the last moved piece,
	 * preset together in any horizontal row. (non-Javadoc) - check if there
	 * exist a group of four similar pieces preset together in any column. -
	 * check if there exist a group of four or more similar pieces preset
	 * together in any left to right/ right to left diagonals.
	 * 
	 * @params - void
	 * 
	 * @ return - void
	 * 
	 * @see Connect4FieldInterface#didLastMoveWin()
	 */
	public boolean didLastMoveWin() {
		int piecesMatched = 0;
		int indexRow = 0;

		/*
		 * look forward and backward from the last move position and calculate
		 * if there exist a group of atleast four similar pieces It handles the
		 * following cases - XXX_ _XXX X_XX XX_X
		 */
		piecesMatched += matchForward(3) + 1;
		if (piecesMatched < 4)
			piecesMatched += matchBackward(4 - piecesMatched);
		if (piecesMatched == 4) {
			return true;
		} else {
			piecesMatched = 0;
		}
		/*
		 * check downwards if there exist a group of four similar pieces present
		 * in the the last move row. It handles the following case : _ X X X
		 */
		if (lastMoveRow + 3 <= boardRows - 1) {
			indexRow = lastMoveRow + 1;
			while (piecesMatched != 3) {
				if (gameBoard[indexRow++][lastMoveCol] == gameBoard[lastMoveRow][lastMoveCol])
					piecesMatched += 1;
				else
					break;
			}
			if (++piecesMatched == 4) {
				return true;
			} else {
				piecesMatched = 0;
			}
		}

		/*
		 * check diagonally from left to right if thre exist a group of four
		 * similar pieces. It handles the following cases :
		 * 
		 * 	_
		 * 	 X
		 *    X
		 *     X
		 *     
		 *  X
		 *   _
		 *    X
		 *     X
		 *  
		 *  X
		 *   X
		 *    _
		 *     X
		 *  
		 *  X
		 *   X
		 *    X
		 *     _
		 */
		piecesMatched += matchDiagonalDown('R', 3) + 1;
		if (piecesMatched < 4)
			piecesMatched += matchDiagonalUp('L', 4 - piecesMatched);
		// }
		if (piecesMatched == 4) {
			return true;
		} else {
			piecesMatched = 0;
		}

		/*
		 * Check diagonally from right to left, and see if there exist a group
		 * of four or more similar pieces. It handles the following cases :
		 * 
		 * 	  _
		 * 	 X
		 *  X
		 * X
		 *     
		 *     X
		 *    _
		 *   X
		 *  X
		 *  
		 *     X
		 *    X
		 *   _
		 *  X
		 *  
		 *     X
		 *    X
		 *   X
		 *  _*/
		piecesMatched += matchDiagonalDown('L', 3) + 1;
		if (piecesMatched < 4)
			piecesMatched += matchDiagonalUp('R', 4 - piecesMatched);
		if (piecesMatched == 4) {
			return true;
		} else {
			piecesMatched = 0;
		}

		return false;
	} // didLastMoveWin

	/*
	 * This method is responsible for finding out the maximum length of a string
	 * of similar pieces present in diagonally upward direction. It works in
	 * both the direction - LEFT and RIGHT based upon the passed in parameter
	 * 
	 * @params - char - direction of diagonal steps - forward steps to look for
	 * 
	 * @return - int - maximum number of similar pieces present together
	 */
	private int matchDiagonalUp(char direction, int steps) {
		int index = 1;
		int counter = 0;
		switch (direction) {
		// find maximum count of similar pieces in diagonally upward right
		// direction
		case 'R':
			while (steps + lastMoveCol > boardColumns - 1
					|| lastMoveRow - steps < 0)
				steps--;
			while (index <= steps && counter <= 3) {
				if (gameBoard[lastMoveRow - index][lastMoveCol + index] == gameBoard[lastMoveRow][lastMoveCol]) {
					counter++;
				} else
					break;
				index++;
			}
			break;
		// find maximum count of similar pieces in diagonally upward left
		// direction
		case 'L':
			while (lastMoveCol - steps < 0 || lastMoveRow - steps < 0)
				steps--;
			while (index <= steps && counter <= 3) {
				if (gameBoard[lastMoveRow - index][lastMoveCol - index] == gameBoard[lastMoveRow][lastMoveCol]) {
					counter++;
				} else
					break;
				index++;
			}
			break;
		}

		return counter;
	} // matchDiagonalUp

	/*
	 * This method is responsible for finding out the maximum length of a string
	 * of similar pieces present in diagonally downward direction. It works in
	 * both the direction - LEFT and RIGHT based upon the passed in parameter
	 * 
	 * @params - char - direction of diagonal steps - forward steps to look for
	 * 
	 * @return - int - maximum number of similar pieces present together
	 */
	private int matchDiagonalDown(char direction, int steps) {
		int index = 1;
		int counter = 0;
		switch (direction) {
		// find maximum count of similar pieces in diagonally downward right
		// direction
		case 'R':
			// cut short the steps if board configuration doesn't allow
			while (steps + lastMoveCol > boardColumns - 1
					|| steps + lastMoveRow > boardRows - 1)
				steps--;
			while (index <= steps && counter <= 3) {
				if (gameBoard[lastMoveRow + index][lastMoveCol + index] == gameBoard[lastMoveRow][lastMoveCol]) {
					counter++;
				} else
					break;
				index++;
			}
			break;
		// find maximum count of similar pieces in diagonally downward left
		// direction
		case 'L':
			while (lastMoveCol - steps < 0
					|| steps + lastMoveRow > boardRows - 1)
				steps--;
			while (index <= steps && counter <= 3) {
				if (gameBoard[lastMoveRow + index][lastMoveCol - index] == gameBoard[lastMoveRow][lastMoveCol]) {
					counter++;
				} else
					break;
				index++;
			}
			break;
		}
		// return the count of similar pieces found together
		return counter;
	} // matchDiagonalDown

	/*
	 * This method moves backward from the position of last move and return back
	 * the count of similar pieces found together.
	 * 
	 * @params - int steps - maximum steps or counter to look backward
	 * 
	 * @return - int - number of similar pieces found connected
	 */
	private int matchBackward(int steps) {
		int counter = 0;
		int index = 1;
		while (lastMoveCol - steps < 0)
			steps--;
		while (index <= steps && counter <= 3) {
			// increase counter is similar pieces are found
			if (gameBoard[lastMoveRow][lastMoveCol - index++] == gameBoard[lastMoveRow][lastMoveCol]) {
				counter++;
			} else
				// break if the current piece is not same as the last move
				break;
		}
		return counter;
	} //matchBackward

	/*
	 * This method moves forward from the position of last move and returns the
	 * count of similar pieces found connected together.
	 * 
	 * @params - int steps - maximum steps or counter to look forward
	 * 
	 * @return - int - number of similar pieces found connected
	 */
	private int matchForward(int steps) {
		int counter = 0;
		int index = 1;
		while (steps + lastMoveCol > boardColumns - 1)
			steps--;
		while (index <= steps && counter <= 3) {
			// increase counter is similar pieces are found
			if (gameBoard[lastMoveRow][lastMoveCol + index++] == gameBoard[lastMoveRow][lastMoveCol]) {
				counter++;
			} else
				// break if the current piece is not same as the last move
				break;
		}
		return counter;
	} // matchForward

	@Override
	/*
	 * This method is responsible for check if the game board is in a draw
	 * condition. It return a TRUE value only if there are no positions on game
	 * board where pieces can be played, and none of the players has won.
	 * (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - boolean - true if no player has won and the board is full.
	 * 
	 * @see Connect4FieldInterface#isItaDraw()
	 */
	public boolean isItaDraw() {
		boolean result = false;
		boolean draw = true;

		// return false if there are empty positions available on the board
		for (int cols = 0; cols < boardColumns; cols++) {
			if (gameBoard[0][cols] == '0')
				return false;
		}

		// check if none of the players has won
		outer: for (int rows = 0; rows < boardRows; rows++) {
			for (int cols = 0; cols < boardColumns; cols++) {
				if (gameBoard[rows][cols] != '0'
						&& gameBoard[rows][cols] != ' ') {
					lastMoveRow = rows;
					lastMoveCol = cols;
					result = didLastMoveWin();
					if (result) {
						draw = false;
						break outer;
					}
				}
			}
		}
		return draw;
	}// isItaDraw

	@Override
	/*
	 * This method is responsible for initializing the game players. It allows
	 * the user to either play against the computer or against another player
	 * (non-Javadoc)
	 * 
	 * @params - PlayerInterface objects - players for the game
	 * 
	 * @see Connect4FieldInterface#init(PlayerInterface, PlayerInterface)
	 */
	public void init(PlayerInterface playerA, PlayerInterface playerB) {

		System.out
				.println("Enter 1 - One player game\nOr any other number for two players game.");
		int onePlayer = inputScanner.nextInt();
		System.out.println("Enter Player Name :\t");
		if (onePlayer == 1)
			// first player is Computer player
			thePlayers[0] = new ComputerPlayer(this);
		else {
			// both the players are human. ask their names and instantiate the
			// objects
			thePlayers[0] = new Player(this, inputScanner.next(), '+');
			System.out.println("Enter Second Player Name :\t");
		}
		thePlayers[1] = new Player(this, inputScanner.next(), '*');
	} // init

	/*
	 * This method overrides the toString method from Object class to print the
	 * current condition of board. It reads the 2D array and displays all the
	 * pieces and empty positions available on the board. (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - String
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String board = "";
		for (int i = 1; i <= boardColumns; i++) {
			System.out.print(i);
			if (i < 10) {
				System.out.print(" ");
			}
		}
		System.out.println();
		for (int rows = 0; rows < boardRows; rows++) {
			for (int cols = 0; cols < boardColumns; cols++) {
				board += gameBoard[rows][cols] + " ";
			}
			board += "\n";
		}
		return board;
	} // toString

	/*
	 * Returns back the board size - No of Rows and Columns of the board.
	 * 
	 * @params - void
	 * 
	 * @return - int[] - integer array with # of rows & columns
	 */
	public int[] getBoardConfig() {
		int[] config = new int[2];
		config[0] = boardRows;
		config[1] = boardColumns;
		return config;

	} // getBoardConfig

	/*
	 * This method returns the top piece present in the selected columns.
	 * 
	 * @params - int - Column number
	 */
	public char getPieceInColumn(int colNo) {
		int rowNo = 0;
		// loop until top piece is reached
		while (rowNo < boardRows && gameBoard[rowNo][colNo - 1] == '0') {
			rowNo++;
		}
		if (rowNo == boardRows)
			rowNo--;

		// return the piece from top or 0 if there is none
		if (gameBoard[rowNo][colNo - 1] == ' ')
			return '0';
		else
			return gameBoard[rowNo][colNo - 1];
	} // getPieceInColumn

	@Override
	/*
	 * This method facilitates two players ( CPU-Player or Player-Player) to
	 * take their turns in an alternate manner based upon the current condition
	 * of the game board. The game continues until one of the player wins or
	 * there is a draw when the board is full with no further moves possible.
	 * (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - void
	 * 
	 * @see Connect4FieldInterface#playTheGame()
	 */
	public void playTheGame() {

		int column;
		boolean gameIsOver = false;
		do {
			for (int index = 0; index < 2; index++) {

				// check if its a draw and abort the game.
				if (isItaDraw()) {
					System.out.println("Game is a draw");
					gameIsOver = true;
				} else {

					// ask for next move of the active player
					column = thePlayers[index].nextMove();

					// drop piece in the above selected position
					dropPieces(column, thePlayers[index].getGamePiece());
					System.out.println("The oponent played in column : "
							+ column);

					// check if the last move resulted in a win, abort the game
					// if true and display the results
					if (didLastMoveWin()) {
						gameIsOver = true;
						System.out.println(this);
						System.out.println("The winner is: "
								+ thePlayers[index].getName());
						break;
					}
				}
				System.out.println(this);
			}

		} while (!gameIsOver);

	} // playTheGame
	
	/*
	 * Program execution starts from the main method.
	 * It creates the instance of a new Connect4 Field
	 * game and starts the game.
	 * 
	 * @params - String[] - command line parameters
	 * @return - void
	 */
	public static void main(String[] args)
	{
		// create instance of new Connect4Field game
		Connect4Field newGame = new Connect4Field(true);
		// play game
		newGame.playTheGame();
	}//main

} // Connect4Field
