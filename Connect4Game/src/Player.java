/*
 * Player.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.util.Scanner;

/**
 * 
 * This class represents a player for the Connect4Field game. Each player
 * instance has a name & a game piece. The class implements PlayerInterface
 * interface and implements its methods.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
 */
public class Player implements PlayerInterface {

	Connect4Field playerGame;
	String playerName;
	char playerPiece;
	Scanner scanner;

	// Constructor for creating the player instance
	public Player(Connect4Field aConnect4Field, String string, char c) {

		// reference of the game board
		playerGame = aConnect4Field;
		playerName = string;
		playerPiece = c;
		scanner = new Scanner(System.in);
	} // Player

	@Override
	/*
	 * This method returns the game piece of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - char - player's game piece (non-Javadoc)
	 * 
	 * @see PlayerInterface#getGamePiece()
	 */
	public char getGamePiece() {
		return playerPiece;
	} // getGamePiece

	@Override
	/*
	 * This method returns the name of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - String - player name (non-Javadoc)
	 * 
	 * @see PlayerInterface#getName()
	 */
	public String getName() {
		return playerName;
	} // getName

	@Override
	/*
	 * This method is responsible for taking the inout from user, representing
	 * the next move of the player. (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - int - column for next move.
	 * 
	 * @see PlayerInterface#nextMove()
	 */
	public int nextMove() {
		int moveCol = 0;
		System.out.println(playerName + "'s move -");
		moveCol = scanner.nextInt();

		// keep asking for new column until a valid move is possible
		while (!playerGame.checkIfPiecedCanBeDroppedIn(moveCol)) {
			System.out.println("Invalid Turn; Try Again");
			moveCol = scanner.nextInt();
		}
		// retun column for next move
		return moveCol;
	} // nextMove

} // Player
