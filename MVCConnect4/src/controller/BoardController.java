package controller;
/*
 * BoardController.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.util.Scanner;

import models.ComputerPlayer;
import models.Connect4Field;
import models.Connect4FieldInterface;
import models.Player;
import models.PlayerInterface;
import views.BoardView;

/**
 * This class acts as the main controller class for the Connect4Field game. It
 * is responsible for the game play order, creation of new players and game
 * board, input processing from the players, error handling and interaction
 * among various other layers of the board game based upon the MVC architecture.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class BoardController {
	/*
	 * Program execution starts from the main method. It creates the instance of
	 * a new Connect4 Field game and starts the game.
	 * 
	 * @params - String[] - command line parameters
	 * 
	 * @return - void
	 */
	public static void main(String[] args) {
		// create instance of new Connect4Field game
		Connect4FieldInterface newGame = new Connect4Field(true);

		// create instance of the new view for calculator
		BoardView view = new BoardView();
		Scanner inputScanner = new Scanner(System.in);

		// initialize the new players for the game
		PlayerInterface[] thePlayers = new PlayerInterface[2];
		view.displayMessage("Enter 1 - One player game\nOr any other number for two players game.");

		int onePlayer = inputScanner.nextInt();
		view.displayMessage("Enter Player Name :\t");
		if (onePlayer == 1)
			// first player is Computer player
			thePlayers[0] = new ComputerPlayer(newGame);
		else {
			// both the players are human. ask their names and instantiate the
			// objects
			thePlayers[0] = new Player(null, inputScanner.next(), '+');
			view.displayMessage("Enter Second Player Name :\t");
		}
		thePlayers[1] = new Player(null, inputScanner.next(), '*');

		// play game
		playTheGame((Connect4Field) newGame, view, thePlayers);

		inputScanner.close();
	}// main

	/*
	 * This method facilitates two players ( CPU-Player or Player-Player) to
	 * take their turns in an alternate manner based upon the current condition
	 * of the game board. The game continues until one of the player wins or
	 * there is a draw when the board is full with no further moves possible.
	 * (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public static void playTheGame(Connect4Field newGame, BoardView view,
			PlayerInterface[] thePlayers) {

		int column;
		boolean gameIsOver = false;
		do {
			for (int index = 0; index < 2; index++) {

				// check if its a draw and abort the game.
				if (newGame.isItaDraw()) {
					if (newGame.checkError() != null) {
						view.displayError(newGame.checkError());
						System.exit(0);
					}

					view.displayDraw();
					gameIsOver = true;
				} else {

					// ask for next move of the active player
					view.displayMessage(thePlayers[index].getName()
							+ "'s move -");
					column = thePlayers[index].nextMove();

					// keep asking for new column until a valid move is possible
					while (!newGame.checkIfPiecedCanBeDroppedIn(column)) {
						view.displayError("Invalid Turn; Try Again");
						column = thePlayers[index].nextMove();
					}

					// drop piece in the above selected position
					newGame.dropPieces(column, thePlayers[index].getGamePiece());
					if (newGame.checkError() != null) {
						view.displayError(newGame.checkError());
						System.exit(0);
					}

					view.displayMove(column);

					// check if the last move resulted in a win, abort the game
					// if true and display the results
					if (newGame.didLastMoveWin()) {
						if (newGame.checkError() != null) {
							view.displayError(newGame.checkError());
							System.exit(0);
						}

						gameIsOver = true;
						view.displayBoard(newGame);
						view.displayWinner(thePlayers[index].getName());
						break;
					}
				}
				view.displayBoard(newGame);
			}

		} while (!gameIsOver);

	} // playTheGame

} // BoardController
