package views;

/*
 * BoardView.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import models.Connect4Field;

/**
 * This class acts as the view for the Connect4Field game. Within the MVC
 * architecture, It is responsible for displaying all messages, errors, and also
 * the board view.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
 */
public class BoardView {

	/*
	 * This method is responsible for displaying the required messages, as
	 * required by the controller.
	 * 
	 * @params - String - message to be displayed.
	 * 
	 * @return - void
	 */
	public void displayMessage(String message) {
		System.out.println(message);
	} // displayMessage

	/*
	 * This method is responsible for dispalying the Connect4Field game board at
	 * any time as requested by the controller.
	 * 
	 * @params - Connect4Field - reference of the game board.
	 * 
	 * @return - void
	 */
	public void displayBoard(Connect4Field board) {
		int j = 0;
		for (int i = 1; i <= board.getBoardConfig()[1]; i++) {
			System.out.print(j++);
			if (i % 10 == 0) {
				j = 0;
			}
		}
		System.out.println();
		System.out.println(board);
	} // displayBoard

	/*
	 * This method is used to display any error messages.
	 * 
	 * @params - String error - error message to be dispalyed.
	 * 
	 * @return - void
	 */
	public void displayError(String error) {
		System.err.println("Error : " + error);
	} // displayError

	/*
	 * This method is used to display the Winner.
	 * 
	 * @params - String - name of the winning player.
	 * 
	 * @return - void
	 */
	public void displayWinner(String winner) {
		System.out.println("The winner is : " + winner);
	} // displayWinner

	/*
	 * Display that the game has ended up in a draw.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void displayDraw() {
		System.out.println("Game is draw");
	} // displayDraw

	/*
	 * Method used to display the last move details.
	 * 
	 * @params - int - column number of last player's move.
	 * 
	 * @return - void
	 */
	public void displayMove(int column) {
		System.out.println("The oponent played in column : " + column);
	} // dispalyMove
} // BoardView
