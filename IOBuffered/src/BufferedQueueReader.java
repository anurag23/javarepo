/*
 * BufferedQueueReader.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * This class represents a non-stoping Input BufferedQueuedReader, which
 * implements a Queue as a buffer storage to fetch data from a file and allow
 * multiple Reader threads to read 1024 bytes at once from buffer.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class BufferedQueueReader extends Thread {
	private BufferQueue buffer;
	private static BufferedInputStream input;
	private static volatile boolean stopReading;

	/**
	 * Create a new instance of BufferedQueueReader internal thread and assign a
	 * buffer to it.
	 * 
	 * @param buffer
	 *            - buffer queue to be filled
	 */
	private BufferedQueueReader(BufferQueue buffer) {
		this.buffer = buffer;
		stopReading = false;
	}

	/**
	 * Create new BufferedQueueReader instance based upon the FileInputStream
	 * and the required buffer size.
	 * 
	 * @param fileInputStream
	 *            - file stream to read from
	 * @param bufferSize
	 *            - buffer size to maintain
	 */
	public BufferedQueueReader(FileInputStream fileInputStream, int bufferSize) {
		// create new buffered input stream reader used by buffer threads to
		// fetch data from file.
		input = new BufferedInputStream(fileInputStream, 1024);
		// set buffer size - where buffer size is NUM * 1024 Bytes
		if (bufferSize > 0)
			buffer = new BufferQueue(bufferSize);
		else
			buffer = new BufferQueue(5);
	}

	/*
	 * Open the BufferedQueueReader instance, this starts the internall buffer
	 * threads to start filling the buffer
	 */
	public void open() {
		// create buffer filling threads
		BufferedQueueReader queueThread = new BufferedQueueReader(buffer);
		queueThread.start();
	}

	/**
	 * Overridden run method, buffer filling threds keeps fetching data from
	 * file until the buffer is full.
	 */
	public void run() {
		byte[] slotData = new byte[1024];
		while (!stopReading) {
			try {
				// for testing // sleep(400);
				// read 1024 bytes from file and save in a temporary slot
				if (input.read(slotData, 0, 1024) == -1) {
					// buffer.add(slotData);
					// append EOF slot
					buffer.addEOF();
					close();
				} else
					// add new slot to buffer queue
					buffer.add(slotData);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Read method allows external calls to read a 1024 Byte slot at once from
	 * the buffer.
	 */
	public byte[] read() {
		return buffer.read();
	}

	/**
	 * Close file reading and buffer filling threads.
	 */
	public void close() {
		try {
			stopReading = true;
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

/*
 * BufferQueue.java
 * 
 * Version: 1.0
 * 
 * Revisions: 1.0
 */
/**
 * This class is used to implement a queue as a buffer for the
 * BufferedQueueReader. The access to shared Queue is synchronized for various
 * buffer filling threads and the reader threads. Each slot in queue is of size
 * 1024 bytes.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
 */
class BufferQueue {
	Slot front, back;
	boolean full;
	int size;
	int MAX_SIZE;
	private static Object queueLock;

	/**
	 * Initialize the BufferQueue instance, provide max_size limit for buffer
	 * 
	 * @param maxSize
	 *            of the buffer
	 */
	public BufferQueue(int maxSize) {
		full = false;
		this.size = 0;
		this.MAX_SIZE = maxSize;
		// create new static lock for buffer
		queueLock = new Object();
	}

	/**
	 * This method is used by buffer filling threads to add new slots into the
	 * buffer.
	 * 
	 * @param data - new slot data to be added
	 *            -
	 */
	public void add(byte[] data) {
		try {
			// create new slot instance
			Slot newSlot = new Slot(data, null);
			synchronized (queueLock) {
				// wait is the buffer is already full
				while (isFull()) {
					System.out.println(" { Writing thread on wait }");
					queueLock.wait();
				}
				// write data to buffer
				System.out.println("*** Writing next slot to buffer ***");
				if (front == null) {
					front = newSlot;
					back = newSlot;
				} else {
					back.setNext(newSlot);
					back = newSlot;
				}
				size++;
				queueLock.notifyAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is accessed by reader threads to fetch data from buffer
	 * @return - first slot data
	 */
	public byte[] read() {
		try {
			synchronized (queueLock) {
				while (isEmpty()) {
					System.out.println("{ Reading thread on wait }");
					queueLock.wait();
				}
				System.out.println("### Reading next slot from buffer ###");
				byte[] data;
				Slot temp = front;
				data = temp.getData();
				front = front.getNext();
				temp.setNext(null);
				size--;
				queueLock.notifyAll();
				return data;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// append EOF at the end
	public void addEOF() {
		add(null);
	}

	
	// check if buffer is empty
	public boolean isEmpty() {
		return size == 0;
	}

	// check if buffer is full
	public boolean isFull() {
		return size == MAX_SIZE;
	}

}

/**
 * Slot class used by the BufferQueue class
 * @author Anurag Malik, am3926
 *
 */
class Slot {
	byte[] data;
	Slot link;

	Slot(byte[] data, Slot link) {
		this.data = data;
		this.link = link;
	}

	// return slot data
	public byte[] getData() {
		return data;
	}

	// return link to next slot
	public Slot getNext() {
		return link;
	}

	// set link to next node
	public void setNext(Slot next) {
		this.link = next;
	}
}
