import java.io.FileInputStream;

public class ReaderTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			byte[] data;

			BufferedQueueReader reader = new BufferedQueueReader(
					new FileInputStream("words.txt"), 10);
			reader.open();
			Thread.sleep(500);
			data = reader.read();
			while (data != null) {
				//do something with data
				//System.out.println(new String(data));
				Thread.sleep(20);
				data = reader.read();
			}
			System.out.println("EOF : Closing reader");
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
