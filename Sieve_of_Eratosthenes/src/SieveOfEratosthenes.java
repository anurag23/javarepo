/*
 * SieveOfErathosthenes.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

/**
 * This program is used to check if a given set of numbers are prime or not. The
 * program uses Sieve Of Erathosthenes technique to determine all the prime
 * numbers within a given range. This program is multithreaded and a command
 * line input parameter can be used to limit the maximum number of threads that
 * can be created at a given point of time during the execution.
 * 
 * @author Anurag Malik, am3926
 */
public class SieveOfEratosthenes implements Runnable {

	final static int FIRSTpRIMEuSED = 2;
	static int MAX;
	// maximum number of threads active at a time
	static int MAX_THREADS;
	final boolean[] numbers;
	public static int index = 1;
	public static int limit;
	static int count;

	// constructor for initializing the number array
	public SieveOfEratosthenes(int max) {
		numbers = new boolean[max];
		MAX = max;
	}

	/*
	 * This method is responsible for setting the default values for all numbers
	 * in the resultant array and also to create the multiple threads
	 * responsible for carrying out the primality test for a position in the
	 * numbers array.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void determinePrimeNumbers() {

		// initialize default values for all numbers
		for (int index = 1; index < MAX; index++) {
			numbers[index] = true;
		}

		// find limit for the division test
		limit = (MAX > 10 ? (int) Math.sqrt(MAX) + 1 : 3);

		// create a new thread group and create new threads when required.
		ThreadGroup sGroup = new ThreadGroup("Prime Nums");
		for (int index = 2; index < limit; index++) {

			// check if the number of active threads is not more that the
			// maximum limit required
			while (sGroup.activeCount() >= MAX_THREADS) {
			}
			Thread x = new Thread(sGroup, this);
			x.start();
		}
	}

	/*
	 * This method is automatically called when a new thread is started. It is
	 * responsible for iterating over the multiples of a number and setting
	 * false for all numbers at those indexes in the resultant array.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 * 
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		synchronized (this) {
			// increment index for next thread
			index++;
			// increment the number of active threads
			count++;
		}

		// set false for all the index which are mulitple of the current number
		if (numbers[index]) {
			int counter = 2;
			while (index * counter < MAX) {
				numbers[index * counter] = false;
				counter++;
			}
		}

		// decrement the number of active threads
		synchronized (this) {
			count--;
		}
	}

	/*
	 * This method is used to provide a list of numbers that are to be tested
	 * for primality test.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void testForPrimeNumber() {
		int[] test = { 2, 3, 4, 7, 13, 17, 24, 31, 1, 0, 54, 81, 87, 101,
				MAX - 1, MAX };
		for (int index = 0; index < test.length; index++) {
			if (test[index] < MAX) {
				System.out.println(test[index] + " = " + numbers[test[index]]);
			}
		}
	}

	/*
	 * Main method is responsible for creating the object for
	 * SieveOfErathosthenes class and run the primality test on a list of given
	 * numbers.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public static void main(String[] args) {

		// set maximum limit for the number of active threads present at a time
		MAX_THREADS = Integer.parseInt(args[0]);

		// create instance of the SieveOfEratosthenes class
		SieveOfEratosthenes aSieveOfEratosthenes = new SieveOfEratosthenes(120);

		// run the multithreaded primality test method
		aSieveOfEratosthenes.determinePrimeNumbers();

		// wait till there are no active threads left
		while (count != 0) {
		}
		// after all threads are completed, run the primality test on a list of
		// numbers
		aSieveOfEratosthenes.testForPrimeNumber();
		System.exit(0);
	}
}