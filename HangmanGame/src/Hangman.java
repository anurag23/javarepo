/*
 * Hangman.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * The aim is to implement a Hangman game. The game can be played by multiple
 * players simultaneously. Each of them are required to guess a word randomly
 * selected from a file. First one to identify the word, or the one with maximum
 * score at the end of game is the winner.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class Hangman {

	/**
	 * 
	 * Program execution starts from the main function. The program expects
	 * following parameters as input from the command line : - location/ address
	 * of the file containing different words. - name of all the players playing
	 * the game. whitespace is treated as a delimiter.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String[] inputParams = args;

		/*
		 * create new session of the HangmanGame and pass the command line
		 * parameters as input
		 */
		HangmanGame newGame = new HangmanGame(inputParams);

		// start the game
		newGame.playGame();

		// show results of the game
		newGame.showResults();
	} // main
} // Hangman

/**
 * 
 * Hangman is a multiplayer game. The aim is to try and guess a word randomly
 * selected by the computer. Each player is allowed to guess the alphabets of
 * the word and each correct guess results in points added to the overall score.
 * A wrong guess on the otherhand results in deduction of points from the
 * overall score.
 * 
 * User is able to see if the current guess was correct or not. Correct
 * characters are displayed at their correct locations in the word. The user is
 * allowed to make 8 wrong guesses, displayed through a hangman figure.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
 */

class HangmanGame {
	private String word, playerName;
	private int noOfPlayers, counter;
	private Player[] allPlayers;
	File wordsFile;

	/**
	 * Construct players (Player class objects) required for the game and get
	 * random word from a file, pass it to all players for the hangman game
	 * session.
	 * 
	 * @param inputParams
	 *            - String[] from command line
	 * @return - None
	 * @exception - None
	 */
	public HangmanGame(String[] inputParams) {

		// find file at the given location
		wordsFile = new File(inputParams[0]);
		noOfPlayers = inputParams.length - 1;

		// ask for multiplayers
		if (noOfPlayers <= 1) {
			System.out.println("This is a multiplayer game, try again.");
			System.exit(0);
		}

		// create required number of Players
		allPlayers = new Player[noOfPlayers];
		counter = 0;

		// get a random word to be guessed from the file
		word = getWord().toLowerCase();

		// provide names and word to each player object
		while (counter < noOfPlayers) {
			playerName = inputParams[counter + 1];
			allPlayers[counter] = new Player(playerName, word);
			counter++;
		}
	} // HangmanGame

	/**
	 * This method scans the file provided and returns a random word from the
	 * file.
	 * 
	 * @param - None
	 * @return - String - random word from a file
	 * @exception - None
	 * 
	 */
	private String getWord() {
		Scanner fileScanner = null;
		try {

			// scan the file, provided at a given location
			fileScanner = new Scanner(wordsFile);
		} catch (FileNotFoundException fileExp) {

			// file not found exception, exit the program
			System.out.println("File not found at the location you entered.");
			System.exit(0);
		}

		// get a random integer from Random class and skip the same number of
		// lines in the file before finally reading the next word from it.
		int randomLine = 0;
		String nextWord = "";
		while (randomLine == 0)
			randomLine = new Random().nextInt(1000);

		while (fileScanner.hasNextLine() && randomLine-- > 0)
			nextWord = fileScanner.nextLine();

		if (nextWord == "") {
			System.out.println("No words found in file");
			System.exit(0);
		}
		fileScanner.close();

		// return the word selected from file
		return nextWord;
	} // getWord

	/**
	 * This method is responsible for providing turns to all the players. It
	 * keeps checking if the status of player is such that he has either made
	 * more than 8 wrong guesses or he has guessed the correct word. It
	 * terminates the current player's turn and allows the next player to take
	 * his turn.
	 * 
	 * @param - None
	 * @return - None
	 * @exception - None
	 */
	public void playGame() {
		System.out.println(word.length() + " letter word to be guessed.");

		// for all players
		for (Player player : allPlayers) {

			// reset the wrong guesses for new player
			int wrongGuess = 0;
			System.out.println(player.playerName + "'s turn :");

			// allow the current player to take turn until he makes 8 or more
			// wrong guesses or he identifies the correct word.
			while (wrongGuess < 8 && !player.getPlayerStatus()) {
				if (!player.takeTurn()) {
					wrongGuess += 1;

					// display the hangman figure for each wrong guess
					displayFigure(wrongGuess);
				}

				// terminate the current player's turn if he has already
				// identified the word.
				if (player.getPlayerStatus())
					break;
			}
			System.out.println();
		}
	} // playGame

	/**
	 * This method simply draws a hangman figure in a 2D array. With each wrong
	 * guess, the hangman figure is updated with another part starting from the
	 * stand to its body.
	 * 
	 * @param wrongGuess
	 * @return - void
	 * @exception - None
	 */
	private void displayFigure(int wrongGuess) {

		// initialize a 20 X 20 - 2D array with all blank spaces.
		String[][] hangman2D = new String[20][20];
		for (int x = 0; x < 20; x++) {
			for (int y = 0; y < 20; y++) {
				hangman2D[x][y] = " ";
			}
		}

		// based upon the number of wrong guesses made by the user
		// update the different sections of the hangman figure.
		switch (wrongGuess) {

		case 8: // draw legs
			hangman2D[15][13] = "/";
			hangman2D[16][11] = "_/";
			hangman2D[15][15] = "\\";
			hangman2D[16][15] = "\\_";

		case 7: // draw hands
			hangman2D[10][13] = "/";
			hangman2D[11][12] = "/";
			hangman2D[10][15] = "\\";
			hangman2D[11][16] = "\\";

		case 6: // draw body
			for (int x = 9; x < 15; x++)
				hangman2D[x][14] = "|";

		case 5: // draw face
			hangman2D[8][12] = "(";
			hangman2D[8][13] = "*_*";
			hangman2D[8][14] = ")";

		case 4: // draw rope
			for (int x = 3; x < 8; x++)
				hangman2D[x][14] = "!";

		case 3: // draw stand
			for (int y = 5; y < 15; y++)
				hangman2D[3][y] = "=";

		case 2: // draw stand
			for (int x = 3; x < 18; x++)
				hangman2D[x][5] = "||";

		case 1: // draw base of the stand
			for (int y = 0; y < 10; y++)
				hangman2D[19][y] = "[]";
			for (int y = 4; y < 6; y++)
				hangman2D[18][y] = "[]";
		}

		// display the updated hangman 2D figure
		for (int x = 0; x < 20; x++) {
			for (int y = 0; y < 20; y++) {
				System.out.print(hangman2D[x][y]);
			}
			System.out.println();
		}
	} // displayFigure

	/**
	 * This method displays the results of all the players. The results are
	 * sorted descendingly based upon the scores of all players.
	 * 
	 * @param - none
	 * @return - void
	 * @exception - none
	 */
	public void showResults() {
		System.out.println();

		// sort players based upon their scores.
		sortLeaderBoard();
		int counter = 0;

		// display results of top 5 players
		for (Player player : allPlayers) {
			if (counter++ > 5)
				break;
			System.out.println(player.playerName + " scored : "
					+ player.getScore());
		}
	} // showResults

	/**
	 * This method is responsible for sorting all the players based upon their
	 * scores. Bubble sorting method is implemented to arrange the players in
	 * order of their descending scores.
	 * 
	 * @param - none
	 * @return - void
	 * @exception - none
	 */
	private void sortLeaderBoard() {
		Player tempPlayer;
		boolean swapping = true;
		int y = 1;

		// keep iterating till the swapping is taking place
		while (swapping) {
			swapping = false;

			// for each iteration, loop on groups of two consecutive players and
			// swap their place if first player's score is less than the
			// second's.
			for (int x = 0; x < allPlayers.length - y++; x++)
				if (allPlayers[x].getScore() < allPlayers[x + 1].getScore()) {
					tempPlayer = allPlayers[x];
					allPlayers[x] = allPlayers[x + 1];
					allPlayers[x + 1] = tempPlayer;
					swapping = true;
				}
		}
	} // sortLeaderBoard
} // HangmanGame

/**
 * 
 * This class represents a Player. Each player object has his name and secret
 * word to be guessed, character by character.
 * 
 * Score card is maintained for each player, such that, for each right guess 10
 * points are added to his score. Whereas for each wrong guess, 5 points gets
 * deducted from his total score. The player is able to see if his current guess
 * was correct or not, and also the correct location of that alphabet in the
 * secret word. A player status flag is maintained to see if the player has
 * already guessed the correct word or not.
 * 
 * @author anurag
 * 
 */
class Player {
	String playerName = null, playerWord = null;
	private String alreadyGuessed = "", wordCopy = "";
	private boolean playerStatus = false;
	private int playerScore = 0;
	private char guess;
	private Scanner playerScanner;

	/**
	 * Constructor - responsible for constructing new players with their names
	 * and word to be guessed.
	 * 
	 * @param - name, word
	 */
	public Player(String name, String word) {

		// player name
		this.playerName = name;

		// player's secret word to be guessed
		this.playerWord = word;

		// copy of the secret word, used to match if the player has guessed the
		// correct word
		wordCopy = playerWord;

		// scanner for taking inputs from the system input stream.
		playerScanner = new Scanner(System.in);
	} // Player

	/**
	 * This method allows the player to take his turn and make a guess of the
	 * next character in the secret word. For each right guess, it calls
	 * rightGuess method to add points to total score else it calls wrong Guess
	 * method to deduce points.
	 * 
	 * For each correct guess, the corresponding alphabets are replaced with a _
	 * in the copy of secret word, which is used to check if whole word has been
	 * identified or not.
	 * 
	 * @return boolean - true for right guess, false for wrong guess
	 */
	public boolean takeTurn() {
		int times = 0;
		boolean hit;

		// make guess, next character in the secret word.
		char guess = makeGuess();

		// if the guessed character is in the secret word.
		if (playerWord.contains("" + guess)) {

			// find all occurences of that character
			for (char chr : playerWord.toCharArray())
				if (chr == guess) {
					times += 1;
				}

			// add the required number of points to the player's total score
			rightGuess(times);

			// replace each correct guess, with a _ in the copy of secret word
			wordCopy = wordCopy.replace(guess, '_');

			// check if the copy of secret word is all identified and matches _
			// string. i.e. the player has identified the word
			if (wordCopy.matches("\\_+")) {
				playerStatus = true;
				System.out.print(playerName + " identified the word : ");
			}

			displayWord();
			hit = true;
		} else {

			// if the player has not already guessed the right word and makes a
			// wrong guess,deduce points from his total score.
			if (!playerStatus)
				wrongGuess();
			hit = false;
		}

		// true for right guess, false for wrong guess
		return hit;
	} // takeTurn

	/**
	 * This method allows the player to make a guess about next characeter in
	 * the secret word and takes input from the system input stream/ keyboard.
	 * 
	 * @param - none
	 * @return - char - guessed character
	 */
	private char makeGuess() {

		// take next input character from keyboard.
		// empty strings are not allowed.
		String input = playerScanner.nextLine();
		while (input.isEmpty()) {
			input = playerScanner.nextLine();
		}

		// if the same input character has already been guessed, ask the player
		// to make another guess.
		while (alreadyGuessed.contains("" + input.charAt(0))) {
			System.out
					.println("You've guessed this already. Try something new.");
			input = playerScanner.nextLine();
			while (input == null || input.isEmpty()) {
				input = playerScanner.nextLine();
			}
		}

		// if player enters multiple characters (string), take only the first
		// character as input
		guess = input.toLowerCase().charAt(0);

		// add the current word to the list of already guessed characters
		alreadyGuessed += guess;

		// return the recent guessed character
		return guess;
	} // makeGuess

	/**
	 * For each right guess, this method adds the appropriate number of points
	 * to the total score of a player. If a character is present more than once
	 * in the secret word, then it adds points for each of them.
	 * 
	 * @param times
	 * @return - void
	 * @exception - none
	 */
	private void rightGuess(int times) {
		if (times == 0) {
			times = 1;
		}

		// add points to the total score of a player
		playerScore += 10 * times;
	} // rightGuess

	/**
	 * For each wrong guess, this method deducts the appropriate number of
	 * points from the total score of a player.
	 * 
	 * @param - none
	 * @return - void
	 * @exception - none
	 */
	private void wrongGuess() {
		playerScore -= 5;
	} // wrongGuess

	/**
	 * This method returns the current score of the player.
	 * 
	 * @param - none
	 * @return int - score of the player
	 * @exception - none
	 */
	public int getScore() {
		return playerScore;
	} // getScore

	/**
	 * This method return the current status of the player. TRUE - if the player
	 * has identified his secret word. FALSE - if the player has not yet
	 * identified his complete secret word.
	 * 
	 * @param - none
	 * @return boolean - player status
	 * @exception - none
	 */
	public boolean getPlayerStatus() {
		return playerStatus;
	} // getPlayerStatus

	/**
	 * This method is used to display the secret secret word every time after a
	 * player makes a correct guess. It displays all the correct guesses and
	 * their position within the secret word.
	 * 
	 * @param - none
	 * @return - none
	 * @exception - none
	 */
	private void displayWord() {

		// for each character in the secret word check if it has already been
		// guessed
		for (char chr : playerWord.toCharArray()) {

			// if the character is already guessed, display it at all correct
			// locations.
			if (alreadyGuessed.contains("" + chr)) {
				System.out.print(chr);
			} else {

				// else hide the characters yet to be guessed
				System.out.print("*");
			}
		}
		System.out.println();

	} // displayWord

} // Player
