

/**
 * This class acts as the view for the calculator based upon the MVC
 * Architecture. It receives and displays the required messages from the main
 * controller class.
 * 
 * @author Anurag Malik, am3926
 */
public class CalculatorView {

	/*
	 * This method printout the required message on the console window.
	 * 
	 * @params - String - message to be displayed
	 * 
	 * @return - void
	 */
	public void displayMessage(String message) {
		System.out.println("Message : " + message);
	} // displayMessage

	/*
	 * This method printout the required result on the console window.
	 * 
	 * @params - String - result to be displayed
	 * 
	 * @return - void
	 */
	public void displayResult(double result) {
		System.out.println("Result : " + result);
	} // displayResult

	/*
	 * This method printout the required error message on the console window.
	 * 
	 * @params - String - error message to be displayed
	 * 
	 * @return - void
	 */
	public void displayError(String error) {
		System.err.println("Error : " + error);
	} // displayError
} // CalculatorView
