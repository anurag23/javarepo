package controller;


/*
 * MainController.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */


import java.util.Scanner;

import views.CalculatorView;
import models.*;

/**
 * This class acts as the main controller class within the MVC architecture of
 * the calculator. It is responsible for handling all the object creation,
 * calculator input output processing and all the interaction between the
 * different layers of the architecture.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class MainController {
	/**
	 * Program execution starts from the main function, it calls the functions
	 * convertToPostfix and calculateExpression to convert the input string into
	 * its postfix notation and then evaluate it.
	 * 
	 * @param String
	 *            [] - Command line parameters
	 * @return void
	 * 
	 */
	public static void main(String[] args) {
		String failure = null;
		CalculatorView view = new CalculatorView();
		view.displayMessage("Please provide input string");

		String[] inputStr = getInput();
		if (inputStr == null)
			view.displayError("Invalid Input");
		// validateInput();

		Calculator newCalc = new Calculator();

		// convert the given expression into its corresponding postfix notation
		MyStack<String> outputStr = newCalc.convertToPostfix(inputStr);

		// check for error and display the error on view if found
		failure = newCalc.checkError();
		if (failure != null) {
			view.displayError(failure);
			System.exit(0);
		}

		// evaluate the value of postfix notation expression
		double result = newCalc.calculateExpression(outputStr);

		// check for error and display the error on view if found
		failure = newCalc.checkError();
		if (failure != null) {
			view.displayError(failure);
			System.exit(0);
		}

		// display results on the view
		view.displayResult(result);

	} // main

	/*
	 * This method is used to take input from system inout stream. The input
	 * should be seperated by ' '.
	 * 
	 * @params - none
	 * 
	 * @return - String[] - array of string operators and operands.
	 */
	private static String[] getInput() {
		Scanner inputScanner = new Scanner(System.in);
		String[] input = inputScanner.nextLine().split(" ");
		inputScanner.close();
		return input;
	} // getInput

} // MainController
