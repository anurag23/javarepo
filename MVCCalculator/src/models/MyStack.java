package models;


import java.lang.reflect.Array;

/*
 * MyStack.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
public class MyStack<TYPE>
{
	/**
	 *
	 * This class is aimed at implementing a user-defined generic stack.
	 * The stack is implemented as a generic array, and it provides the
	 * following five methods - 
	 * 	- push : method to push the input value at the end of the stack
	 * 	- pop : method to remove and return the value at the top of the stack
	 *	- getItem : get the value stored in stack at an index passed as input
	 *	- isEmpty : return boolean true if the stack is empty and 
	 *				no value is stored in it, else returns false
	 *	- size : returns the size of stack i.e. index up to which 
	 *			 there are values stored in stack 
	 *
	 * @author Anurag Malik, am3926
	 * 
	 */
	
	//generic array
	TYPE[] myStack;

	public MyStack(int sizeOfStack, Class<TYPE[]> data)
	{
		//instantiate the generic array based upon the input data type and size
		myStack = data.cast(Array.newInstance(data.getComponentType(), sizeOfStack));
	}
	
	//push value at the top of stack
	public void push(TYPE item)
	{
		int size = size();
		myStack[size] = item;
	}
	
	//remove & return the last value stored on top of stack
	public TYPE pop()
	{
		int len = size();
		TYPE value = getItem(len-1);
		myStack[len-1] = null;
		return value;
	}
	
	//get value stored at an index in stack
	public TYPE getItem(int index)
	{
		return	myStack[index];	
	}
	
	//return size in terms of number of items stored in stack
	public int size()
	{
		int size=0;
		try{
			while(getItem(size++) != null) {}
		} catch (ArrayIndexOutOfBoundsException e) { }
		if (size == 0)
			return 0;
		else
			return size-1;
	}
	
	//return true if stack is empty, else return false
	public boolean isEmpty()
	{	
		//check if first element in array is null
		if(myStack[0] == null)
			return true;
		else
			return false;
	}	
} //MyStack

