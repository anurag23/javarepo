package models;
/*
 * Calculator.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

/**
 * 
 * The aim is to implement a calculator program capable of evaluating values of
 * math expressions using a predefined precedence order of operators. User is
 * allowed to provide a mathematical expression through command line. Input
 * expression from the user is first converted into its corresponding postfix
 * expression.
 * 
 * Two AraryList's are used for the purpose to stack implementation. One of
 * which is used to store the operators and reordering them based upon the
 * precedence order being followed.
 * 
 * The Postfix expression is then evaluated to provide the final result.
 * MyStack(s) are used for the purpose of stack implementation required to hold
 * the temporary intermediate values.
 * 
 * The whole architecture of the Calculator is based upon the MVC architecture.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Calculator {
	// global string to store any failure/ error
	String FAILURE = null;
	// global variable to store predefined precedence order
	static String precedenceOrder;

	/*
	 * Constructor for setting the desired precedence order for the calculator.
	 * 
	 * 
	 * Global variable string 'precedenceOrder' is used to store which
	 * precedence order of operators is to be followed to evaluate the string.
	 * boolean expression true==true can be changed to true==false to toggle
	 * between the following two & vice versa : 1.+ 2.- 3.% 4.* 5./ 6.^ or 1.^
	 * 2./ 3.* 4.% 5.+ 6.-
	 */
	public Calculator() {

		precedenceOrder = "";
		if (true == true)
			precedenceOrder = "+-%*/^";
		else
			precedenceOrder = "^/*%+-";
	}

	/**
	 * Evaluate the Postfix expression being passed in as the parameter. The
	 * evaluation process is as follows : - Start reading the input expression
	 * from left to right. - If the next token is a numerical value, push it
	 * onto the operands stack (valueStack) - If the next token is a left
	 * parentheses, simply push it onto operators stack - If the next token is
	 * an operator, pop the last two values from the operands stack and evaluate
	 * them using the current operator. Push back the result onto the operands
	 * stack. - If the next token is a right parentheses, keep popping the
	 * operators from the operators stack onto operands stack until a
	 * corresponding left parentheses in not found. - Follow the above three
	 * steps. Until all of the input string is traversed. - Final value left in
	 * the operands stack is returned as the result.
	 * 
	 * @param outputStr
	 *            (Input Postfix expression)
	 * @return opResult - evaluated value of the postfix expression
	 * @exception none
	 */
	public double calculateExpression(MyStack<String> outputStr) {

		double opResult = 0;
		double firstOperand, secondOperand;
		String token;
		MyStack<Double> valueStack = new MyStack<Double>(outputStr.size(),
				Double[].class);
		int inputSize = outputStr.size();
		for (int i = 0; i < inputSize; i++) {

			token = outputStr.getItem(i);
			// push numerical values onto operand stack
			if (token.matches("(\\-)?\\d+(\\.\\d+)?"))
				valueStack.push(Double.parseDouble(token));

			// perform operation on top two values stored on the operand stack
			else {
				secondOperand = valueStack.pop();
				firstOperand = valueStack.pop();

				// switch to required operation
				switch (token) {
				case "+": // addition
					opResult = firstOperand + secondOperand;
					break;
				case "-": // subtraction
					opResult = firstOperand - secondOperand;
					break;
				case "*": // multiplication
					opResult = firstOperand * secondOperand;
					break;
				case "/": // division - Catch Division by Zero exception
					try {
						opResult = firstOperand / secondOperand;
					} catch (Exception e) {
						setError("Divide by Zero exception");
						return (0);
					}
					break;
				case "^": // exponential power of a number
					opResult = Math.pow(firstOperand, secondOperand);
					break;
				case "%": // modulus function
					opResult = firstOperand % secondOperand;
					break;

				// display error and exit if an unidentified operator is found
				default:
					setError("Unidentified Operator");
					return (0);
				}

				// add result on the top of stack
				valueStack.push(opResult);
				opResult = 0;
			}
		}

		// return the last value left in the operand stack
		return valueStack.getItem(0);

	} // calculateExpression

	/**
	 * 
	 * This method is used to convert the input string from user, into its
	 * corresponding Postfix expression. Following process is followed to
	 * evaluate the postfix notation : - Read each token from the input string -
	 * If the next token is a numeric value, add it to the postfix notation
	 * (outputStr) - If the next token is an operator - - If the operators stack
	 * is empty, push current operator onto it. - Repeat if the operator stack
	 * is not empty, compare its precedence with the top most operator on stack.
	 * - If the precedence of current token is less, pop the last operator and
	 * push it onto outputStr - Else push the current operator onto operator
	 * stack. - For each operator left in operation stack, push them onto
	 * outputStr - Return outputStr
	 * 
	 * @param inputStr
	 *            (Input string from Command Line)
	 * @return outputStr (MyStack<String>)
	 * @exception none
	 */

	public MyStack<String> convertToPostfix(String[] inputStr) {
		MyStack<String> outputStr = new MyStack<String>(inputStr.length + 1,
				String[].class);
		MyStack<String> operatorStack = new MyStack<String>(
				inputStr.length + 1, String[].class);

		// boolean variable to keep a check if an operand is missing
		boolean operatorAgain = false;

		// boolean variable to keep a check if an operator is missing
		boolean operandAgain = false;
		boolean firstToken = true;
		for (String token : inputStr) {
			/*
			 * regEx expression to match for integers and decimal point numbers
			 * (+/-)
			 */
			if (token.matches("(\\-)?\\d+(\\.\\d+)?")) {
				if (operandAgain == true) {
					setError("Invalid expression - Missing operator");
					return null;
				}

				// add the current token to the resultant postfix expression
				outputStr.push(token);

				operatorAgain = false;
				operandAgain = true;

			} else {

				if (operatorAgain == true) {
					if (!token.matches("\\(")) {
						// exit if an operand is found missing in input
						// expression
						setError("Invalid expression - Missing Operand");
						return null;
					}
				} else {
					if (token.matches("\\(") && firstToken == false) {
						setError("Invalid expression - Missing Operator");
						return null;
					}
				}

				if (operatorStack.isEmpty())

					// push the current token onto operators stack
					operatorStack.push(token);
				else {
					int len = operatorStack.size();
					if (!token.matches("\\)")) {

						/*
						 * compare the precedence of current input token to the
						 * token on top of stack
						 */
						while (!(operatorStack.isEmpty())
								&& getPrecedence(operatorStack.getItem(len - 1)) > getPrecedence(token)
								&& !token.matches("\\(")) {
							outputStr.push(operatorStack.pop());
							len = operatorStack.size();
						}

						// push the current token onto operators stack
						operatorStack.push(token);
					} else {
						/*
						 * if a right parentheses is found, start popping the
						 * operators onto output stack - postfix notation until
						 * a corresponding left parentheses is not found
						 * 
						 * raise error if operators stack empties but no
						 * corresponding left parentheses is found
						 */
						while (!operatorStack.isEmpty()
								&& !operatorStack.getItem(len - 1).matches(
										"\\(")) {
							outputStr.push(operatorStack.pop());
							len = operatorStack.size();
						}
						if (operatorStack.isEmpty()) {
							setError("Unbalanced left parantheses");
							return null;
						}
						operatorStack.pop();
					}
				}

				if (!token.matches("\\)"))
					operatorAgain = true;
				operandAgain = false;
			}
			firstToken = false;
		}

		String token;
		while (!operatorStack.isEmpty()) {
			token = operatorStack.pop();
			/*
			 * Start popping left out operators & raise error if a left
			 * parentheses is found
			 */
			if (token.matches("\\(")) {
				setError("Unbalanced right parantheses");
				return null;
			}
			// push operators on output stack
			outputStr.push(token);
		}
		// return the final postfix notation expression
		return outputStr;
	} // convertToPostFix

	/**
	 * This method returns the value of precedence order of the input operand,
	 * based upon a predefined precedence order.
	 * 
	 * @param token
	 * @return integer value equivalent to the index of a character in the
	 *         precedence order string.
	 * @exception none
	 */
	public int getPrecedence(String token) {

		// return the index of a character in the precedenceOrder string
		return precedenceOrder.indexOf(token);

	} // getPrecedences

	/**
	 * This method is used to check if there has been an error and return the
	 * corresponding error message.
	 * 
	 * @params - void
	 * @return - String - error message
	 */
	public String checkError() {
		return FAILURE;
	} // checkFailure

	/**
	 * This method is used to raise an error flag and set an error message
	 * 
	 * @param String
	 *            - error message
	 * @return void
	 */
	private void setError(String error) {
		FAILURE = error;
	} // setError

} // Calculator