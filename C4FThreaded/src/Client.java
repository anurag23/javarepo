/*
 * Client.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * This class acts as a client application capable of connecting to online
 * server at a giver host address and port number.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Client extends Thread {
	private volatile boolean serverLost = false;
	String name;
	static Thread reader, writer;
	static PrintWriter out;
	static BufferedReader in;
	static Scanner scr;

	/**
	 * Constructor for initializing a new client thread
	 * 
	 * @param name
	 */
	Client(String name) {
		this.name = name;
	}

	/**
	 * Overridden method from the Thread class, responsible for conitnuosly
	 * reading from the socket input stream and also writting to the socket
	 * output stream.
	 */
	public void run() {

		try {
			while (!serverLost) {
				// reader thread - read from input stream until null or
				// GAME_OVER message is received
				if (name.equals("READ")) {
					String data = in.readLine();
					if (data == null || data.equals("GAME_OVER")) {
						System.out.println("Connection to server lost");
						return;
					}
					System.out.println(data);
				} else {
					// writting thread - keep writing to the output stream
					// check if EXIT command is given at any time.
					String data = scr.nextLine();
					if (!data.equals("EXIT"))
						out.println(data);
					else {
						serverLost = true;
						return;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

	/**
	 * Main method for creating connection with the required server. It creates
	 * two threads for reading and writting to the socket I/O Streams
	 * respectively.
	 * 
	 * @param args - HOSTNAME and PORT of the server
	 */
	public static void main(String[] args) {
		try {
			Socket socket = new Socket(args[0], Integer.parseInt(args[1]));
			System.out.println("Client online !!"
					+ "\nEnter EXIT anytime to end session.");
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			scr = new Scanner(System.in);

			reader = new Client("READ");
			writer = new Client("WRITE");
			reader.start();
			writer.start();
			reader.join();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
