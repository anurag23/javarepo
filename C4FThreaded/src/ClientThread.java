/*
 * ClientThread.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This class extends thread and can be used to creat threads responsible for
 * handling communication with a new online user. It asks the details of new
 * player and assigns it to the correct waiting list for Connect4Field game.
 * 
 * @author Anurag Malik, am3926
 */
public class ClientThread implements Runnable {
	Socket socket;
	GameServer server;

	ClientThread(Socket socket, GameServer server) {
		this.socket = socket;
		this.server = server;
	}

	@Override
	/**
	 * Overridden method from Thread class, here it is responsible for communicating and 
	 * asking details from new user trying to conenct to the Connect4Field game server. 
	 * It then creates new Player instances.
	 * 
	 */
	public void run() {
		PrintWriter out = null;
		BufferedReader in = null;
		String inData;
		try {
			// get output stream and input stream conenctions from the socket
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			// ask which game to play
			out.println("Welcome to Connect4Field Online!! \nPress 2 for two player game."
					+ "\nPress 4 for four player game.");
			inData = in.readLine();
			if (inData == null) {
				// if null is received, exit connection from this user
				System.out.println("Lost connection : "
						+ socket.getLocalAddress());
				socket.close();
				return;
			}

			int option = Integer.parseInt(inData);
			while (option != 2 && option != 4) {
				out.println("Wrong Input." + "\nPress 2 for two player game."
						+ "\nPress 4 for four player game.");
				inData = in.readLine();
				if (inData == null) {
					System.out.println("Lost connection : "
							+ socket.getLocalAddress());
					socket.close();
					return;
				}
				option = Integer.parseInt(inData);
			}

			// ask name of the new player
			out.println("What's your name?");
			String name = in.readLine();

			// add the new player to the appropriate two player or four player
			// list on the server.
			switch (option) {
			case 2:
				if (!server.canTwoPlayNext()) {
					out.println(name
							+ "!! You'll have to wait for other players to join a two player game.");
				}
				server.addTwoPlayerQueue(new Player(socket, name));
				break;
			case 4:
				if (!server.canFourPlayNext()) {
					out.println(name
							+ "!! You'll have to wait for other players to join a four player game.");
				}
				server.addFourPlayerQueue(new Player(socket, name));
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
