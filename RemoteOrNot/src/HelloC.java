/*
 * HelloC.java
 * 
 * Version : 1.0
 * Revision : 1.0
 *
 * Author : Anurag Malik, am3926
 * 
 */
import java.net.InetAddress;
import java.rmi.Naming;
import java.util.Scanner;

/**
 * This class is used to perform a test using RMI in Java, to test weather a
 * method is executed locally or on a remote server.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class HelloC {

	/*
	 * This method call a test method in the shared HelloInterface, which
	 * returns the Hostname of the machine it is being run upon. A mismatch of
	 * hostname is regarded as Remote invocation else same hostname refers to
	 * local execution of the test method.
	 * 
	 * params : HelloInterface instance to be tested
	 */
	public static void localRemoteTest(HelloInterface obj) {
		try {
			// call test method on the input object and test if the hostname is
			// same as local machine or it is a remote machine
			if (InetAddress.getLocalHost().getHostName().toString()
					.equals(obj.test())) {
				System.out.println("Executed on LOCAL MACHINE");
			} else {
				System.out.println("Executed on REMOTE MACHINE");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * program execution starts from the main method which performs the local or
	 * remote execution test on two object of the same class and implementing
	 * the same interface, hostname is used to connect to the server and receive
	 * a stub object. Another object of the same class is created locally.
	 */
	public static void main(String args[]) {
		try {
			//ask for server hostname
			Scanner scr = new Scanner(System.in);
			System.out.println("Enter host name of server : ");
			String hostname = scr.nextLine();
			// create a local object
			localRemoteTest(new HelloImplementation());
			// retrieve a remote obejct through RMI call.
			localRemoteTest((HelloInterface) Naming
					.lookup("rmi://" +hostname +":" + 1099
							+ "/hello"));
			scr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	} // main.java
} // HelloC.java