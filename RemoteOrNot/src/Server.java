/*
 * Server.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag Malik, am3926
 */
import java.rmi.Naming;

public class Server {
	/*
	 * this method creates a new server instance and starts it.
	 */
	public static void main(String args[]) {
		Server server = new Server();
		server.start();
	}

	public void start() {
		String registryURL;
		try {
			// register a new HelloImplementation remote object with the RMI
			// Registry
			registryURL = "rmi://anurag-xps.wireless.rit.edu:" + 1099
					+ "/hello";
			HelloInterface obj = new HelloImplementation();
			Naming.rebind(registryURL, obj);
			
			System.out.println("New object registered");
		} catch (Exception re) {
			System.out.println("Exception on Server: " + re);
		}
	}
} // Server
