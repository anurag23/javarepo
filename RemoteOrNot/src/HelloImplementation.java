/*
 * HelloImplementation.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag Malik, am3926
 */
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * This class is responsible for implementing the hellointerface and provide
 * definition of the test method, which returns the name of the host machine it
 * is being executed upon.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class HelloImplementation extends UnicastRemoteObject implements
		HelloInterface {

	/*
	 * constructor for initializing the new instances
	 */
	protected HelloImplementation() throws RemoteException {
		super();
	}

	@Override
	/*
	 * This method is used to return back the host name of the system.
	 * 
	 */
	public String test() throws RemoteException {
		try {
			return InetAddress.getLocalHost().getHostName().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	} //test
} // HelloImplementation
