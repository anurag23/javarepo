// original from: http://rosettacode.org/wiki/Mandelbrot_set#Java
// modified for color

/*
 * Mandelbrot.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;

/**
 * This class creates an image of the Mandlebrot on a Jrame canvas. The program
 * is multithreaded and maximum number of threads is dependent upon the
 * processors present on the host system.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Mandelbrot extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int MAX = 1000;
	private final int LENGTH =400;
	private final double ZOOM = 200;
	private BufferedImage theImage;

	// colour array
	private int[] colors = new int[MAX];

	// maximum number of threads that are allowed to be created at a time
	// private static int THREADS_LIMIT;
	// count of active threads
	// private static int count;
	// maximum number of threads created during execution

	// constructor - responsible for setting the JFrame window co-ordinates and
	// default other options. It also fetches the maximum number of cores
	// available in the CPU, to set the upper limit on the maximum number of
	// threads that can be created at a given point of time during the execution
	public Mandelbrot() {
		super("Mandelbrot Set");

		// initialize colour array
		initColors();
		setBounds(100, 100, LENGTH, LENGTH);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		theImage = new BufferedImage(getWidth(), getHeight(),
				BufferedImage.TYPE_INT_RGB);

		// get CPU cores count
		// THREADS_LIMIT = Runtime.getRuntime().availableProcessors();
	}

	/*
	 * this method is responsible for creating a number of threads each
	 * associated with a pixel on the canvas. Each thread than executes in its
	 * own run method and set the RGB colour value for that pixel.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void assignTask() {

		int count = 10;
		ServiceThread[] serviceThreads = new ServiceThread[count];
		for (int i = 0; i < count; i++) {
			serviceThreads[i] = new ServiceThread(0, i, this);
			serviceThreads[i].setName("Thread 0" + i);
			serviceThreads[i].start();
		}

		boolean taskAssigned = false;
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				taskAssigned = false;
				while (!taskAssigned) {
					for (ServiceThread t : serviceThreads) {
						if (taskAssigned)
							break;
						taskAssigned = t.giveNewTask(x, y);
					}
				}
			}
		}

		for (ServiceThread t : serviceThreads) {
			t.stopProcess();
		}
	}

	/*
	 * This method is used to fill an array of colours later used to pick pixel
	 * colours.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void initColors() {
		for (int index = 0; index < MAX; index++) {
			colors[index] = Color.HSBtoRGB(index / 256f, 1, index
					/ (index + 8f));
		}
	}

	/*
	 * This method is responsible for painting/ displaying the JFrame
	 * components. Here, It is responsible for printing the image after all the
	 * threads has finished setting the pixel RGB values.
	 * 
	 * @params - void
	 * 
	 * @return - void (non-Javadoc)
	 * 
	 * @see java.awt.Window#paint(java.awt.Graphics)
	 */
	public void paint(Graphics g) {
		g.drawImage(theImage, 0, 0, this);
	}

	public void setRGB(int xcord, int ycord, int iter) {
		// set RGB value of a pixel at a given co-ordinate

		if (iter > 0)
			theImage.setRGB(xcord, ycord, colors[iter]);
		else
			theImage.setRGB(xcord, ycord, iter | (iter << 8));
		//System.out.println("Pixel set : " + xcord + "," + ycord);
	}

	public double[] getConfig() {
		return (new double[] { LENGTH, ZOOM, MAX });
	}

	/*
	 * This method is the implementation on run method required for a Runnable
	 * interface. Here, it is responsible for calculating the RGB value for each
	 * individual pixel and setting the colour on image component. It retreives
	 * detail of co-ordinates from the current thread's name and works on it.
	 * 
	 * @params - void
	 * 
	 * @return - void (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */

	/*
	 * Main method is responsible for creating the Mandelbrot instance and
	 * finally printing out the image on JFrame canvas.
	 * 
	 * @params - String[] args - command line arguments
	 */
	public static void main(String[] args) {
		Mandelbrot aMandelbrot = new Mandelbrot();
		aMandelbrot.setVisible(true);
		aMandelbrot.assignTask();
		aMandelbrot.repaint();
	}
}
