class ServiceThread extends Thread {
	volatile int xcord, ycord;
	Mandelbrot mandelBrot;
	volatile boolean workCompleted = false;
	Object lock = new Object();

	public ServiceThread(int xCord, int yCord, Mandelbrot mandel) {
		this.xcord = xCord;
		this.ycord = yCord;
		mandelBrot = mandel;
	}

	public void run() {
		double[] config = mandelBrot.getConfig();
		double zx, zy, cX, cY;
		while (!workCompleted) {

			//System.out.println(this.getName() + " : Starting with X,Y : "
			//		+ xcord + "," + ycord);
			zx = zy = 0;
			cX = (xcord - config[0]) / config[1];
			cY = (ycord - config[0]) / config[1];
			int iter = 0;
			double tmp;
			while ((zx * zx + zy * zy < 10) && (iter < config[2] - 1)) {
				tmp = zx * zx - zy * zy + cX;
				zy = 2.0 * zx * zy + cY;
				zx = tmp;
				iter++;
			}
			mandelBrot.setRGB(xcord, ycord, iter);
			synchronized (lock) {
				try {
					// sleep(2000);
					//System.out.println(this.getName() + " : Waiting");
					workCompleted = true;
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean giveNewTask(int x, int y) {
		if (workCompleted) {
			synchronized (lock) {
				xcord = x;
				ycord = y;
				workCompleted = false;
				lock.notify();
				return true;
			}
		} else
			return false;
	}

	public void stopProcess() {
		synchronized (lock) {
			workCompleted = true;
			lock.notify();
		}
	}

}