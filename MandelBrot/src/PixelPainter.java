import java.awt.image.BufferedImage;


public class PixelPainter extends Thread {
	int xcord, ycord;
	static int count;
	volatile int LENGTH;
	volatile int ZOOM;
	BufferedImage image;
	
	PixelPainter(int xcord, int ycord, int length, int zoom, BufferedImage image) {
		this.xcord = xcord;
		this.ycord = ycord;
		LENGTH = length;
		ZOOM = zoom;
		this.image = image;
	}
	
	public void run(){
		
		String[] cords = Thread.currentThread().getName().split(",");
		int xcord = Integer.parseInt(cords[0]);
		int ycord = Integer.parseInt(cords[1]);

		// calculate the factor for deciding RGB value of a pixel
		double zx, zy, cX, cY;
		zx = zy = 0;
		cX = (xcord - LENGTH) / ZOOM;
		cY = (ycord - LENGTH) / ZOOM;
		int iter = 0;
		double tmp;
//		while ((zx * zx + zy * zy < 10) && (iter < MAX - 1)) {
			tmp = zx * zx - zy * zy + cX;
			zy = 2.0 * zx * zy + cY;
			zx = tmp;
			iter++;
//		}

		// set RGB value of a pixel at a given co-ordinate
//		if (iter > 0)
//			//theImage.setRGB(xcord, ycord, colors[iter]);
//		else
//			theImage.setRGB(xcord, ycord, iter | (iter << 8));

	}
}
