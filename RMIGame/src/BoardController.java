/*
 * BoardController.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class acts as the main controller class for the Connect4Field game. It
 * is responsible for the game play order, creation of new players and game
 * board, input processing from the players, error handling and interaction
 * among various other layers of the board game based upon the MVC architecture.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class BoardController extends Thread {
	static int count;
	Connect4Field newGame;
	BoardView view;
	PlayerInterface[] thePlayers;
	char[] pieces = { '*', '+', '@', 'x' };
	int playerCount;

	/**
	 * Constructor for initializing the new mulitplayer game. List of players is
	 * received and saved in an array of players. Different game pieces are
	 * provided all the players.
	 * 
	 * @param playerCount
	 * @param playersList
	 */
	public BoardController(int playerCount, ArrayList<Player> playersList) {
		// create instance of new Connect4Field game
		newGame = new Connect4Field(true);
		this.playerCount = playerCount;
		// create instance of the new view for calculator
		view = new BoardView();

		int len = playersList.size();
		// initialize the new players for the game
		thePlayers = new PlayerInterface[len];
		int index = 0;
		if (len == 1) {
			thePlayers[0] = new ComputerPlayer(newGame);
			index = 1;
		}

		// provide game pieces to all players
		for (int i = index; i < len; i++) {
			thePlayers[i] = playersList.get(i);
			thePlayers[i].setGamePiece(pieces[i]);
		}
		++count;
	}

	/*
	 * Overridden method from Thread class, responsible for starting the new
	 * Connect4Field game. (non-Javadoc)
	 */
	public void run() {
		int instance = count;
		System.out.println("Game : " + instance + " started");
		playTheGame();
		System.out.println("Game : " + instance + " closed");
	}

	/*
	 * This method facilitates two players ( CPU-Player or Player-Player) to
	 * take their turns in an alternate manner based upon the current condition
	 * of the game board. The game continues until one of the player wins or
	 * there is a draw when the board is full with no further moves possible.
	 * (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void playTheGame() {

		int column;
		boolean gameIsOver = false;
		boolean gameExit = false;

		for (int i = 0; i < playerCount; i++) {
			// display conenction details and details of all players to each
			// other
			view.displayMessage(thePlayers[i], thePlayers[i].getName()
					+ "!! Your game piece is : " + thePlayers[i].getGamePiece()
					+ "\nYou are playing against :");
			for (int j = 0; j < playerCount; j++) {
				if (i != j)
					view.displayMessage(thePlayers[i],
							thePlayers[j].getName() + ", playing with : "
									+ thePlayers[i].getGamePiece() + "\n");
			}
			// display board to all players
			view.displayBoard(thePlayers[i], newGame);
		}
		do {
			for (int index = 0; index < playerCount; index++) {

				// check if its a draw and abort the game.
				if (newGame.isItaDraw()) {
					if (newGame.checkError() != null) {
						view.displayError(thePlayers[index],
								newGame.checkError());
						System.exit(0);
					}

					// display draw details to all the players
					for (PlayerInterface player : thePlayers)
						view.displayDraw(player);
					gameIsOver = true;
				} else {

					// ask for next move of the active player
					for (PlayerInterface player : thePlayers) {
						view.displayMessage(player, thePlayers[index].getName()
								+ "'s move");
					}

					// get move from next player.
					column = thePlayers[index].nextMove();
					if (column == -1) {
						// if -1 is received, then connection to current player
						// has been lost.
						String exitMessage = "Connection to "
								+ thePlayers[index].getName()
								+ " lost.\n Exiting game.";

						// show exit message to all other players and end game.
						System.out.println(exitMessage);
						for (int i = 0; i < thePlayers.length; i++) {
							if (i != index) {
								view.displayMessage(thePlayers[i], exitMessage);
								view.displayMessage(thePlayers[i], "GAME_OVER");
							}
						}
						gameExit = true;
						break;
					}
					// keep asking for new column until a valid move is possible
					while (!newGame.checkIfPiecedCanBeDroppedIn(column)) {
						view.displayError(thePlayers[index],
								"Invalid Turn; Try Again");
						column = thePlayers[index].nextMove();
					}

					// drop piece in the above selected position
					newGame.dropPieces(column, thePlayers[index].getGamePiece());
					if (newGame.checkError() != null) {
						view.displayError(thePlayers[index],
								newGame.checkError());
						System.exit(0);
					}

					// display move to all players except the one who played
					// last move
					for (int i = 0; i < thePlayers.length; i++) {
						if (i != index)
							view.displayMove(thePlayers[i], thePlayers[index],
									column);
					}

					// check if the last move resulted in a win, abort the game
					// if true and display the results
					if (newGame.didLastMoveWin()) {
						if (newGame.checkError() != null) {
							view.displayError(thePlayers[index],
									newGame.checkError());
							System.exit(0);
						}

						gameIsOver = true;

						for (PlayerInterface player : thePlayers) {
							view.displayBoard(player, newGame);
							view.displayWinner(player,
									thePlayers[index].getName());
						}
						break;
					}
				}
				// diaply board to all players
				for (PlayerInterface player : thePlayers) {
					view.displayBoard(player, newGame);
				}
			}
		} while (!gameIsOver && !gameExit);

		if (!gameExit)
			for (PlayerInterface player : thePlayers) {
				view.displayMessage(player, "GAME_OVER");
			}
	} // playTheGame
	
} // BoardController
