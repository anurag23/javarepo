
public interface PlayerInterface {

	public char getGamePiece();
	public String getName();
	public int  nextMove();
	public void setGamePiece(char ch);
	public void view(String message);
}
