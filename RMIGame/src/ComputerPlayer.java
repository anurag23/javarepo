

/**
 * 
 * This class represents a computer player for the Connect4Field game. The class
 * implements PlayerInterface interface and implements its methods. It is
 * responsible for evaluating the next move for computer based upon the current
 * configuration of game board.
 * 
 * @author Anurag Malik, am3926
 * 
 */

public class ComputerPlayer implements PlayerInterface {

	Connect4Field playerGame;
	String playerName;
	char playerPiece;
	int boardColumns = 0;

	// Constructor for creating the computer player instance
	public ComputerPlayer(Connect4FieldInterface aConnect4Field) {
		playerGame = (Connect4Field)aConnect4Field;
		playerName = "Computer";
		playerPiece = '+';

		// get board configuration
		boardColumns = playerGame.getBoardConfig()[1];
	} // ComputerPlayer

	@Override
	/*
	 * This method returns the game piece of the computer player instance
	 * 
	 * @params - void
	 * 
	 * @return - char - computer's game piece (non-Javadoc)
	 * 
	 * @see PlayerInterface#getGamePiece()
	 */
	public char getGamePiece() {
		return playerPiece;
	} // getGamePiece

	@Override
	/*
	 * This method returns the computer name
	 * 
	 * @params - void
	 * 
	 * @return - String - computer name (non-Javadoc)
	 * 
	 * @see PlayerInterface#getName()
	 */
	public String getName() {
		return playerName;
	} // getName

	@Override
	/*
	 * This method is responsible for calculating the next move of the computer
	 * based upon the following strategy -
	 * 
	 * - Check if there is a possible move on board that leads to a win for
	 *   computer. 
	 * - Otherwise, check if there is a possible move that results in
	 *   a win for human player 
	 * - Otherwise, check if there exist a pattern like _X_X_ on the board 
	 *   that may lead to a sure win for the human player on next move.
	 * - Otherwise, check if there is position avaialble on board resulting
	 *   in a _X_X_ pattern then make this move.
	 * - Otherwise, find first avialable move from left on board and make
	 *   that move.
	 * 
	 * @params - void
	 * @return - int - column for next move
	 * @see PlayerInterface#nextMove()
	 */
	public int nextMove() {
		int moveCol = 1;
		boolean canWin = false, playerCanWin = false, riskCondition = false;
		boolean trickTurn = false;

		// check if there is a possible move resulting in a win
		for (int colNo = 1; colNo <= boardColumns; colNo++) {
			if (playerGame.checkIfPiecedCanBeDroppedIn(colNo)) {
				playerGame.dropPieces(colNo, playerPiece);
				if (playerGame.didLastMoveWin()) {
					moveCol = colNo;
					canWin = true;
				}
				playerGame.undoLastMove();

				if (canWin) {
					break;
				}
			}
		}

		// check if there is a possible next move for the player to move 
		// and block it.
		if (!canWin) {
			for (int colNo = 1; colNo <= boardColumns; colNo++) {
				if (playerGame.checkIfPiecedCanBeDroppedIn(colNo)) {
					playerGame.dropPieces(colNo, '*');
					if (playerGame.didLastMoveWin()) {
						moveCol = colNo;
						playerCanWin = true;
					}
					playerGame.undoLastMove();

					if (playerCanWin) {
						break;
					}
				}
			}
			// check if there is a _X_X_ pattern on board that may result in sure win
			// for the player on next move, block it 
			if (!playerCanWin) {
				for (int colNo = 2; colNo <= boardColumns - 3; colNo++) {
					if (playerGame.getPieceInColumn(colNo) == '*'
							&& playerGame.getPieceInColumn(colNo + 2) == '*'
							&& playerGame.getPieceInColumn(colNo + 1) == '0') {
						moveCol = colNo + 1;
						riskCondition = true;
						break;
					}
				}
				// check if there exist a _X___ pattern and take next move 2 steps ahead 
				//of the last piece aiming for the _X_X_ pattern 
				if (!riskCondition) {
					for (int colNo = 2; colNo <= boardColumns - 3; colNo++) {
						if ((playerGame.getPieceInColumn(colNo) == getGamePiece())
								&& playerGame.getPieceInColumn(colNo + 1) == '0') {
							if (playerGame.getPieceInColumn(colNo + 2) == '0'
									&& playerGame.getPieceInColumn(colNo + 3) == '0') {
								moveCol = colNo + 2;
								trickTurn = true;
								break;
							} else {
								moveCol = colNo + 1;
								trickTurn = true;
								break;
							}
						}
					}
					
					// check if there are 5 free spaces available, move at its second 
					// position and aim for creating _X_X_ pattern
					int counter = 0;
					if (!trickTurn) {
						for (int colNo = 1; colNo < boardColumns; colNo++) {
							counter = 0;
							while (colNo + counter < 25
									&& playerGame.getPieceInColumn(colNo
											+ counter) == '0' && counter <= 4) {
								counter++;
							}
							if (counter == 5) {
								moveCol = colNo + 1;
								trickTurn = true;
								break;
							}
						}
					}
					
					// if none of the strategies are applicable, find the first 
					// available position on the board starting from left
					if (!trickTurn) {
						for (int colNo = 1; colNo <= boardColumns; colNo++) {
							if (playerGame.checkIfPiecedCanBeDroppedIn(colNo)) {
								moveCol = colNo;
								break;
							}
						}
					}
				}
			}
		}
		return moveCol;
	} // nextMove

	@Override
	public void setGamePiece(char ch) {
		this.playerPiece = ch;
	}

	@Override
	public void view(String message) {
		// TODO Auto-generated method stub
		
	}

} // ComputerPlayer
