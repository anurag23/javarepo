

/*
 * BoardView.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */


/**
 * This class acts as the view for the Connect4Field game. Within the MVC
 * architecture, It is responsible for displaying all messages, errors, and also
 * the board view.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class BoardView {

	/*
	 * This method is responsible for displaying the required messages, as
	 * required by the controller.
	 * 
	 * @params - String - message to be displayed.
	 * 
	 * @return - void
	 */
	public void displayMessage(PlayerInterface player, String message) {
		player.view(message);
	} // displayMessage

	/*
	 * This method is responsible for dispalying the Connect4Field game board at
	 * any time as requested by the controller.
	 * 
	 * @params - Connect4Field - reference of the game board.
	 * 
	 * @return - void
	 */
	public void displayBoard(PlayerInterface player, Connect4Field board) {
		String header = "";
		int j = 0;
		for (int i = 1; i <= board.getBoardConfig()[1]; i++) {
			header += j++;
			if (i % 10 == 0) {
				j = 0;
			}
		}
		player.view(header + "\n"+ board.toString());
	} // displayBoard

	/*
	 * This method is used to display any error messages.
	 * 
	 * @params - String error - error message to be dispalyed.
	 * 
	 * @return - void
	 */
	public void displayError(PlayerInterface player, String error) {
		player.view("Error : " + error);
	} // displayError

	/*
	 * This method is used to display the Winner.
	 * 
	 * @params - String - name of the winning player.
	 * 
	 * @return - void
	 */
	public void displayWinner(PlayerInterface player, String winner) {
		player.view("The winner is : " + winner);
	} // displayWinner

	/*
	 * Display that the game has ended up in a draw.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void displayDraw(PlayerInterface player) {
		player.view("Game is draw");
	} // displayDraw

	/*
	 * Method used to display the last move details.
	 * 
	 * @params - int - column number of last player's move.
	 * 
	 * @return - void
	 */
	public void displayMove(PlayerInterface player, PlayerInterface played, int column) {
		player.view(played.getName() + " played in column : " + column);
	} // dispalyMove
} // BoardView
