/*
 * GameServer.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class acts as an online game server for Connect4Field game.It accepts
 * requests from various clients and then group multiple online user into either
 * 2 player or 4 player games as requested. The server is multi threaded and can
 * accept a large number of connections from various users at the same time.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class GameServer extends Thread {

	static ServerSocket serverSocket;
	static final int PORT = 4040;
	static Object twoLock = new Object();
	static Object fourLock = new Object();
	static ArrayList<Player> twoPlayersList;
	static ArrayList<Player> fourPlayersList;
	static volatile boolean serverStop;
	Thread tExec, t2game, t4game;
	String name;

	/*
	 * Constructor for initializing the game server
	 */
	public GameServer() {
		serverStop = false;
		// create 2 player and 4 player array lists, for storing all requests.
		twoPlayersList = new ArrayList<Player>();
		fourPlayersList = new ArrayList<Player>();
	}

	/**
	 * Game server starting method - this method starts various threads within
	 * the server. EXEC - threads with name EXEC are the main execution threads
	 * responsible for listening to client requests and creating corresponding
	 * ClientThread.
	 * 
	 * GAME2 - this thread keeps reading on the twoPlayerList and starts a new 2
	 * player game thread if full.
	 * 
	 * GAME4 - this thread keeps reading from the fourPlayerList and starts a
	 * new 4 player game thread if full.
	 */
	public void startServer() {
		// start server and register a new remote object
		execServer();
		// tExec = new GameServer("EXEC");
		t2game = new GameServer("GAME2");
		t4game = new GameServer("GAME4");
		// tExec.start();
		t2game.start();
		t4game.start();
	}

	// Server thread name
	private GameServer(String name) {
		this.name = name;
	}

	/**
	 * Overridden method from the Thread class, here it is divides the various
	 * tasks amongst the three different main threads running on the server.
	 */
	public void run() {
		switch (name) {
		case "GAME2":
			// thread responsible for keeping a check on 2 player games
			// arraylist and start new game whenever 2 players are available
			runTwoPlayerGameManager();
			break;
		case "GAME4":
			// thread responsible for keeping a check on 4 player games
			// arraylist and start new game whenever 4 players are available
			runFourPlayerGameManager();
		}
	}

	/**
	 * This method is executed in theads, and it is responsible for creating a
	 * new two player game, whenever sufficient players are available.
	 */
	private void runTwoPlayerGameManager() {
		while (!serverStop) {
			try {
				synchronized (twoLock) {
					// less than 2 players, then wait
					if (twoPlayersList.size() < 2)
						twoLock.wait();
					if (serverStop)
						break;
					// start new 2 player Connect4Field game in a new thread
					System.out.println("New 2 Player game started");
					new BoardController(2, twoPlayersList).start();

					// clear the players list for new players
					twoPlayersList.clear();
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * This method is executed in theads, and it is responsible for creating a
	 * new four player game, whenever sufficient players are available.
	 */
	private void runFourPlayerGameManager() {
		while (!serverStop) {
			try {
				synchronized (fourLock) {
					// less than 4 players, then wait
					if (fourPlayersList.size() < 4)
						fourLock.wait();
					if (serverStop)
						break;
					// start a new 4 player Connect4field game in a new thread.
					System.out.println("New 4 Player game started");
					new BoardController(4, fourPlayersList).start();
					fourPlayersList.clear();
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * This method is used to stop the server at any instance using END command
	 * through the system default input stream.
	 */
	private void stopServer() {
		serverStop = true;
		try {
			// close server socket
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// notify other two threads to wake-up and end
		synchronized (twoLock) {
			twoLock.notify();
		}
		synchronized (fourLock) {
			fourLock.notify();
		}
	}

	/**
	 * This method allows the ClientThreads to check if the current player can
	 * be added to 2 players list and new game can be started or not.
	 * 
	 * @return True if there is already a player waiting for 2 player game
	 */
	public boolean canTwoPlayNext() {
		synchronized (twoLock) {
			if (twoPlayersList.size() == 1) {
				return true;
			}
			return false;
		}
	}

	/**
	 * This method allows the ClientThreads to check if the current player can
	 * be added to 4 players list and new game can be started or not.
	 * 
	 * @return True if there are already 3 players waiting for a 4 player game
	 */
	public boolean canFourPlayNext() {
		synchronized (fourLock) {
			if (fourPlayersList.size() == 3) {
				return true;
			}
			return false;
		}
	}

	/**
	 * This synchronized method allows the ClientThreads to add new players to
	 * the waiting queue for 2 player game.
	 * 
	 * @param player
	 */
	public void addTwoPlayerQueue(Player player) {
		synchronized (twoLock) {
			// add new player to the fourPlayersList
			twoPlayersList.add(player);
			// if there are 2 players available, notify the game starting
			// thread.
			if (twoPlayersList.size() == 2)
				twoLock.notify();
		}
	}

	/**
	 * This synchronized method allows the ClientThreads to add new players to
	 * the waiting queue for 4 player game.
	 * 
	 * @param player
	 */
	public void addFourPlayerQueue(Player player) {
		synchronized (fourLock) {
			// add new player to the fourPlayersList
			fourPlayersList.add(player);

			// if there are 4 players available, notify the game starting
			// thread.
			if (fourPlayersList.size() == 4)
				fourLock.notify();
		}
	}

	/**
	 * This method is used to create a new instance of the ServerLogic remote
	 * object, which acts as the interface to different clients. RMI is used to
	 * register the new object to RMI registry.
	 */
	public void execServer() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			int portNum = 1099;
			System.out.println("Enter Server host name :");
			// get port number
			ServerLogic exportedObj = new ServerLogic(this);
			String registryURL = "rmi://"+ (br.readLine()).trim()+":" + portNum
					+ "/connect";
			// register the new object with RMI registry
			Naming.rebind(registryURL, exportedObj);
			System.out.println("Conenct4Filed server ready : "
					+ InetAddress.getLocalHost().getHostAddress());
		}
		catch (Exception re) {
			System.out.println("Exception in GameServer : " + re);
		}
	} // execServer

	/**
	 * Main program for starting the Connect4Field online game server. It
	 * creates a new instance of the GameServer class.
	 * 
	 * @param args
	 *            - commandline arguments
	 */

	public static void main(String[] args) {
		GameServer server = new GameServer();
		// start the server, start the internal threads
		server.startServer();
		System.out.println("Server up and running, Enter END to stop anytime.");
		Scanner scr = new Scanner(System.in);
		// read next line from the server input stream, stop server of END
		// command is read.
		while (!scr.nextLine().equals("END")) {
		}
		scr.close();
		server.stopServer();
	}

}
