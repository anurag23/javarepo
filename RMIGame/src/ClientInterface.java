/*
 *ClientInterface.java
 *
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag Malik, am3926 
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote {
	void pushMessage(String message) throws RemoteException;

	public String getAddress() throws RemoteException;

	public String getInput() throws RemoteException;
}
