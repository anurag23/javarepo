/*
 * ServerInterface.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag Malik, am3926
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for Connect4Field game server.
 * @author Anurag Malik
 *
 */
public interface ServerInterface extends Remote {
	void register(ClientInterface client) throws RemoteException;
}
