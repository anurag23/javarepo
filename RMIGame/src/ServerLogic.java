/*
 * ServerLogic.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag Malik, am3926
 */
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * This class provides the implementation of the ServerInterface, including the
 * register method for allowing the new clients to register themselves for
 * callback mechanism.
 * 
 * @author Anurag Malik
 * 
 */
public class ServerLogic extends UnicastRemoteObject implements ServerInterface {
	GameServer server;

	protected ServerLogic(GameServer server) throws RemoteException {
		super();
		this.server = server;
	}

	/*
	 * This method is responsible for creating new client handler threads.
	 * (non-Javadoc)
	 * @see ServerInterface#register(ClientInterface)
	 */
	public synchronized void register(ClientInterface client)
			throws RemoteException {
		System.out.println("New Connection request from : "
				+ client.getAddress());
		try {
			new ClientThread(client, server).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	} // register
} // ServerLogic
