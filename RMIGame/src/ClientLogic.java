/*
 * ClientLogic.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * author : Anurag Malik, am3926
 */
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

/**
 * This class provides implementation for the ClientInterface, which allows the
 * server to callback and start communicating with the client.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class ClientLogic extends UnicastRemoteObject implements ClientInterface {
	Scanner scr;

	protected ClientLogic() throws RemoteException {
		super();
		scr = new Scanner(System.in);
	}

	/**
	 * This method is used by the server to push data/ messages onto the client.
	 */
	@Override
	public void pushMessage(String message) throws RemoteException {
		System.out.println(message);
	} // pushMessage

	/**
	 * This method is responsible for returning back the host adress of the
	 * client machine.
	 */
	@Override
	public String getAddress() throws RemoteException {
		try {
			return InetAddress.getLocalHost().getHostAddress().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	} // getAddress

	/**
	 * This method is used by the server to ask for new data from the system
	 * default input.
	 */
	@Override
	public String getInput() throws RemoteException {
		return scr.nextLine();
	} // getInput
} // ClientLogic
