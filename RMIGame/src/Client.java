/*
 * Client.java
 * 
 * Version : 1.0
 * Revision : 1.0
 * 
 * Author : Anurag malik, am3926
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.Naming;

/**
 * This class represents a client for connecting to online game server for
 * Conenct4field game, through Remote Method invocation.
 * 
 * @author Anurag Malik
 * 
 */
public class Client {

	public static void main(String args[]) {
		try {
			String hostName;
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.println("Enter Server host name:");
			hostName = reader.readLine();
			System.out.println("Enter Server port number:");
			int portNum = Integer.parseInt(reader.readLine());
			// connect to the conenct 4 field game server.
			String registryURL = "rmi://" + hostName + ":" + portNum
					+ "/connect";
			ServerInterface server = (ServerInterface) Naming
					.lookup(registryURL);
			System.out.println("Conencted");

			// Create a new object of the ClientLogic class and register it for
			// the callback from server.
			ClientInterface callbackObj = new ClientLogic();
			server.register(callbackObj);
			System.out.println("Registered for callback.");
		} catch (Exception e) {
			System.out.println("Exception in client: " + e);
		}
	} // main
}// Client
