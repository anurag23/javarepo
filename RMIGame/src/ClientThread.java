/*
 * ClientThread.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This class extends thread and can be used to creat threads responsible for
 * handling communication with a new online user. It asks the details of new
 * player and assigns it to the correct waiting list for Connect4Field game.
 * 
 * @author Anurag Malik, am3926
 */
public class ClientThread extends Thread {
	ClientInterface client;
	GameServer server;

	ClientThread(ClientInterface client, GameServer server) {
		this.client = client;
		this.server = server;
	}

	@Override
	/**
	 * Overridden method from Thread class, here it is responsible for communicating and 
	 * asking details from new user trying to conenct to the Connect4Field game server. 
	 * It then creates new Player instances.
	 * 
	 */
	public void run() {
		try {
			// ask which game to play
			client.pushMessage("Welcome to Connect4Field Online!! \nPress 2 for two player game."
					+ "\nPress 4 for four player game.");
			String input = client.getInput();
			if (input == null) {
				// if null is received, exit connection from this user
				System.out.println("Lost connection");
				return;
			}

			int option = Integer.parseInt(input);
			while (option != 2 && option != 4) {
				client.pushMessage("Wrong Input."
						+ "\nPress 2 for two player game."
						+ "\nPress 4 for four player game.");
				input = client.getInput();
				if (input == null) {
					System.out.println("Lost connection");
					return;
				}
				option = Integer.parseInt(input);
			}

			// ask name of the new player
			client.pushMessage("What's your name?");
			String name = client.getInput();

			// add the new player to the appropriate two player or four player
			// list on the server.
			switch (option) {
			case 2:
				if (!server.canTwoPlayNext()) {
					client.pushMessage(name
							+ "!! You'll have to wait for other players to join a two player game.");
				}
				server.addTwoPlayerQueue(new Player(client, name));
				break;
			case 4:
				if (!server.canFourPlayNext()) {
					client.pushMessage(name
							+ "!! You'll have to wait for other players to join a four player game.");
				}
				server.addFourPlayerQueue(new Player(client, name));
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
