

/*
 * Player.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * 
 * This class represents a player for the Connect4Field game. Each player
 * instance has a name & a game piece. The class implements PlayerInterface
 * interface and implements its methods.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Player implements PlayerInterface {

	Connect4Field playerGame;
	String playerName;
	char playerPiece;
	Scanner scanner;
	ClientInterface client;
	private BufferedReader in;
	private PrintWriter out;

	// Constructor for creating the player instance
	public Player(Connect4Field aConnect4Field, String string, char c) {

		// reference of the game board
		playerGame = aConnect4Field;
		playerName = string;
		playerPiece = c;
		scanner = new Scanner(System.in);
	} // Player

	public Player(ClientInterface client, String name) {
		try {
			this.client = client;
			playerName = name;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	/*
	 * This method returns the game piece of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - char - player's game piece (non-Javadoc)
	 * 
	 * @see PlayerInterface#getGamePiece()
	 */
	public char getGamePiece() {
		return playerPiece;
	} // getGamePiece

	public void setGamePiece(char piece) {
		this.playerPiece = piece;
	} // getGamePiece

	@Override
	/*
	 * This method returns the name of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - String - player name (non-Javadoc)
	 * 
	 * @see PlayerInterface#getName()
	 */
	public String getName() {
		return playerName;
	} // getName

	@Override
	/*
	 * This method is responsible for taking the inout from user, representing
	 * the next move of the player. (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - int - column for next move.
	 * 
	 * @see PlayerInterface#nextMove()
	 */
	public int nextMove() {
		int moveCol = 0;
		try {
			moveCol = Integer.parseInt(client.getInput());
		} catch (NumberFormatException | IOException e) {
			System.out.println("Connection to " + playerName + " lost");
			return -1;
		}
		// retun column for next move
		return moveCol;
	} // nextMove

	@Override
	public void view(String message) {
		try {
			client.pushMessage(message);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
} // Player
