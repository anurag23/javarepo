/*
 * StringZipOutputStream.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * This class is capable of reading an input file and create a compressed input
 * using the LempelZivWelch (LZW) compression technique. It reads lines from
 * input file and start building its dictionary of compression codes. These
 * compressed codes are then replaced in the compressed output file.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 */
public class StringZipOutputStream {

	private int buffer; // buffer to store output bits
	private int bufferCount; // count of bits present in the buffer
	private BufferedOutputStream outputStream; // output stream - points to
												// output file
	private LZWHashMap lzwMap; // Hash map for storing the compressed code keys
	private final int bitSize = 12; // number of bits to pack data
	private int keyCount = 0; // new hash map keys

	// default constructor - takes output to standard system output
	public StringZipOutputStream() {
		outputStream = new BufferedOutputStream(System.out);
	}

	// constructor to initialize output stream
	public StringZipOutputStream(FileOutputStream filename) {
		outputStream = new BufferedOutputStream(filename);

		// create empty hash map and add first 256 primitive characters to it.
		lzwMap = new LZWHashMap(1024);
		for (int i = 0; i < 256; i++)
			lzwMap.put("" + (char) i, i);
	}

	/*
	 * This method converts the required compression code into its binary
	 * representation and add it to the output buffer. It takes as input the
	 * size of number of bits the output representation should be.
	 * 
	 * @params - number = compressed code to be sent to output
	 * 
	 * @params - size = integer the number of bits for representing each code.
	 * 
	 * @return - void
	 */
	public void writeToOutput(int number, int size) {
		if (number < 0)
			// return on error
			return;

		// if the input code is within 0-255, append the required number of
		// zeros for its proper bit represention in SIZE
		if (number < 256) {
			for (int i = 0; i < size - 8; i++) {
				writeBits(false);
			}
			size = 8;
		}

		// write each bit of the compressed code number to the buffer
		for (int i = 0; i < size; i++) {
			// parse next bit from number
			boolean bit = ((number >>> (size - i - 1)) & 1) == 1;
			// write next bit to buffer
			writeBits(bit);
		}
	}

	/*
	 * This method writes the next boolean to the buffer. It keeps a check if
	 * the buffer is full then write it to the output stream.
	 * 
	 * @params - boolean bit - bit to be written to output stream.
	 * 
	 * @return - void
	 */
	public void writeBits(boolean bit) {
		// one left shift in buffer
		buffer <<= 1;
		// append 1 or 0
		if (bit)
			buffer |= 1;

		// increase counter of bits in buffer
		bufferCount++;
		// check if the buffer is full, write to file if full
		if (bufferCount == 8)
			writeBytesToFile();
	}

	/*
	 * This method writes the bits in a buffer (as bytes) to the output stream.
	 * 
	 * @params - void
	 * 
	 * @return - coid
	 */
	public void writeBytesToFile() {

		// if buffer is not full, fill it with zeros at the end
		if (bufferCount > 0)
			buffer <<= (8 - bufferCount);
		try {
			// write byte to the output stream
			outputStream.write(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// reset buffer and buffer count
		bufferCount = 0;
		buffer = 0;
	}

	/*
	 * This method reads a line and compresses it using the LZW Compression. LZW
	 * Compression builds and refers to a hash map for the purpose of storing
	 * and retrieving compress codes for various repetitive substrings with the
	 * line. The LZW Hashmap keeps building its vocabulary of compressed codes
	 * as more and more lines are parsed for compression.
	 * 
	 * @params - String - Line to be compressed and sent to output stream.
	 * 
	 * @return - void
	 */
	public void write(String line) {
		int index = 0;
		String buffer = "";
		String key = "";

		// return if the line to be compressed is null
		if (line == null || line.equals(""))
			return;

		// pick first character form the line, add it to string buffer
		buffer += line.charAt(index++);
		// traverse thw whole input string and send for compression.
		for (char nextChar : line.substring(1).toCharArray()) {
			// create key from buffer and next character
			key = buffer + nextChar;

			// search if the key is in the hash map
			if (lzwMap.contains(key))
				buffer = key;
			else {
				// else write the new key to the hash map and send data in
				// buffer for the compression
				writeToOutput(lzwMap.get(buffer), bitSize);
				if (keyCount < 3500)
					lzwMap.put(key, 256 + keyCount++);
				buffer = "" + nextChar;
			}
		}

		// write data in buffer to output stream and append a new line
		// character.
		writeToOutput(lzwMap.get(buffer), bitSize);
		writeToOutput('\n', bitSize);
	}

	/*
	 * Flush any remaining data to the output stream and close it.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void close() {
		writeBytesToFile();
		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
