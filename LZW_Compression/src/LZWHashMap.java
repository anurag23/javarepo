/*
 * LZWHashMap.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

/**
 * Hashmap imlementation capable of storing key value pairs into linked list
 * records. Key - String Value - Integer
 * 
 * @author Anurag Malik, am3926
 */
public class LZWHashMap {
	String key;
	int value;
	Record[] records;
	int size;

	public LZWHashMap(int size) {
		records = new Record[size];
		this.size = size;
	}

	// Single record
	class Record {
		String key;
		int value;
		Record next;

		// initialize record
		public Record(String key, int value, Record next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}

		// return key
		public String getKey() {
			return key;
		}

		// return value
		public int getValue() {
			return value;
		}

		// return pointer to next record
		public Record getNext() {
			return next;
		}
	}

	// add new record to the hash map
	public void put(String newKey, int value) {
		if (newKey == null)
			return;

		int hash = Math.abs(newKey.hashCode()) % size;
		Record newEntry = new Record(newKey, value, null);

		if (records[hash] == null)
			records[hash] = newEntry;
		else {
			Record previous = null;
			Record iterator = records[hash];

			while (iterator != null) {
				if (iterator.getKey().equals(newKey)) {
					if (previous == null) {
						newEntry.next = iterator.getNext();
						records[hash] = newEntry;
						return;
					} else {
						newEntry.next = iterator.getNext();
						previous.next = newEntry;
						return;
					}
				}
				previous = iterator;
				iterator = iterator.getNext();
			}
			previous.next = newEntry;
		}
	}

	// get value of a record with in the hash map, search using key
	public int get(String key) {
		int hash = Math.abs(key.hashCode()) % size;
		if (records[hash] == null) {
			return -1;
		} else {
			Record node = records[hash];
			while (node != null) {
				if (node.getKey().equals(key))
					return node.value;
				node = node.next;
			}
			return -1;
		}
	}

	// check if a record with a particular key is present in the hash map
	public boolean contains(String key) {
		int hash = Math.abs(key.hashCode()) % size;
		if (records[hash] == null) {
			return false;
		} else {
			Record node = records[hash];
			while (node != null) {
				if (node.getKey().equals(key))
					return true;
				node = node.next;
			}
			return false;
		}
	}
}
