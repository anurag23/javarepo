/*
 * StringZipInputStream.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * This class is capable of reading from an input file compressed using the
 * LempelZivWelch (LZW) compression technique and provide un-compressed input.
 * It reads lines from input file and start building its dictionary of
 * de-compression codes. These compressed codes are then replaced with original
 * data for the uncompressed output data.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 */
public class StringZipInputStream {

	private static final int EOF = -1; // variable representing the end-of-file
	private int buffer; // buffer for storing byte data from input file
	private int bufferCount; // count of bits in buffer

	// fixed bit representation size. This should match with the bit size used
	// to represent codes while compression
	private final int bitSize = 12;
	private BufferedInputStream in;
	private String[] lzwMap; // array for storing de-compression codes

	// counter for number of de-compression codes created
	private int keyCount = 0;

	public StringZipInputStream() {
		in = new BufferedInputStream(System.in);
	}

	/*
	 * constructor - responsible for initializing the input stream reading from
	 * compressed file. It also initializes an array for storing LZW
	 * de-compression codes.
	 * 
	 * @params - FileInputStream - path of compressed input file
	 */
	public StringZipInputStream(FileInputStream filename) {
		in = new BufferedInputStream(filename);

		// initialize array with codes from 0-255
		lzwMap = new String[4000];
		for (int i = 0; i < 256; i++)
			lzwMap[keyCount++] = "" + (char) i;
	}

	/*
	 * This method is responsible for reading bits from the input compressed
	 * file and return compressed codes of 'size' - bit representation
	 * 
	 * @params - integer Size - number of bits used to represent the code while
	 * compression
	 * 
	 * @return - integer - compressed code
	 */
	private int readCode(int size) {
		int x = 0;

		// read 'size' number of bits from the input file and create
		// corresponding integer code.
		for (int i = 0; i < size; i++) {
			x <<= 1;
			boolean bit = readBits();
			if (bit)
				x |= 1;
		}
		return x;
	} // readCode

	/*
	 * This method reads next single bit from the buffer and check if the buffer
	 * is empty to refill it again.
	 * 
	 * @params - void
	 * 
	 * @return - boolean - next bit from the buffer
	 */
	private boolean readBits() {
		if (isEmpty())
			return false;

		// check if the buffer is empty, refill it from next byte from file if
		// required
		if (bufferCount == 0)
			refillBuffer();

		// fetch and return next bit from the buffer
		bufferCount--;
		boolean bit = ((buffer >> bufferCount) & 1) == 1;
		return bit;
	} // readBits

	/*
	 * This method reads next byte from the input stream and refills the buffer.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	private void refillBuffer() {
		try {
			// read next byte from input stream, check if the end of file is
			// found
			buffer = in.read();
			bufferCount = 8;
		} catch (IOException e) {
			System.err.println("EOF");
			// set flag for end of file
			buffer = EOF;
		}
	} // refillBuffer

	/*
	 * This method reads compressed data from the input file and decompresses it
	 * using the LZW de-compression method. An array of decompressed codes is
	 * build on the run.
	 * 
	 * @params - void
	 * 
	 * @return - decompressed next line from the input file
	 */
	public String read() {
		String nextLine = "";

		// read next compressed code from the input file
		int nextCode = readCode(bitSize);

		// break if next line is found
		if (nextCode == (int) '\n')
			return nextLine;

		// get string code from the LZW array map
		String val = lzwMap[nextCode];

		while (true) {
			// append decompressed value to the output
			nextLine += val;

			// read next code from the file
			nextCode = readCode(bitSize);
			if (nextCode == (int) '\n')
				break;

			// return back null if the end of file is found
			if (buffer == EOF)
				return null;

			// get string value of code from the LZW map
			String s = lzwMap[nextCode];
			if (keyCount == nextCode)
				s = val + val.charAt(0);

			// create new entry for decompression code into the LZW maps
			if (keyCount < 3500 + 256)
				lzwMap[keyCount++] = val + s.charAt(0);
			val = s;
		} // read

		// append decompressed data to output
		return nextLine + '\n';
	}

	/*
	 * Close connection from the input compressed file.
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public void close() {
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // close

	/*
	 * Check if the buffer is empty
	 * 
	 * @params - void
	 * 
	 * @return - void
	 */
	public boolean isEmpty() {
		return buffer == EOF;
	} // isEmpty
} // StringInputStream
