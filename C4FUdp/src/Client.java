/*
 * Client.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * This class acts as a client application capable of connecting to online
 * server at a giver host address and port number.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Client extends Thread {
	private volatile boolean serverLost = false;
	String name;
	static Thread reader, writer;
	static PrintWriter out;
	static BufferedReader in;
	static Scanner scr;
	static DatagramSocket socket;
	InetAddress serverIP = null;
	int serverPort = 4040;

	/**
	 * Constructor for initializing a new client thread
	 * 
	 * @param name
	 */
	Client(String name) {
		this.name = name;
	}

	/**
	 * Overridden method from the Thread class, responsible for continuosly
	 * reading from the socket input stream and also writting to the socket
	 * output stream.
	 */
	public void run() {
		try {
			serverIP = InetAddress.getByName("129.21.108.194");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		try {
			while (!serverLost) {
				// reader thread - read from input stream until null or
				// GAME_OVER message is received
				if (name.equals("READ")) {
					byte[] receiveData = new byte[1024];
					DatagramPacket packet = new DatagramPacket(receiveData,
							receiveData.length);
					socket.receive(packet);
					String data = new String(packet.getData());
					if (data == null || data.equals("GAME_OVER")) {
						System.out.println("Connection to server lost");
						return;
					}
					System.out.println(data);
				} else {
					// writting thread - keep writing to the output stream
					// check if EXIT command is given at any time.
					String data = scr.nextLine();
					if (!data.equals("EXIT")) {
						byte[] buffer = data.getBytes();
						DatagramPacket packet = new DatagramPacket(buffer,
								buffer.length, serverIP, serverPort);
						try {
							socket.send(packet);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						serverLost = true;
						return;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

	/**
	 * Main method for creating connection with the required server. It creates
	 * two threads for reading and writting to the socket I/O Streams
	 * respectively.
	 * 
	 * @param args
	 *            - HOSTNAME and PORT of the server
	 */
	public static void main(String[] args) {
		try {
			socket = new DatagramSocket();
			System.out.println("Client online !!"
					+ "\nEnter EXIT anytime to end session.");
			scr = new Scanner(System.in);

			reader = new Client("READ");
			writer = new Client("WRITE");
			reader.start();
			System.out.println("Enter 1/2/4 game choice and your NAME.");
			writer.start();
			reader.join();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
