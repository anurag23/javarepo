

/*
 * Player.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */


/**
 * 
 * This class represents a player for the Connect4Field game. Each player
 * instance has a name & a game piece. The class implements PlayerInterface
 * interface and implements its methods.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class Player implements PlayerInterface {

	String playerName;
	char playerPiece;
	ClientThread myHandler;

	public Player(ClientThread clientThread, String name) {
		myHandler = clientThread;
		playerName = name;
	}

	@Override
	/*
	 * This method returns the game piece of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - char - player's game piece (non-Javadoc)
	 * 
	 * @see PlayerInterface#getGamePiece()
	 */
	public char getGamePiece() {
		return playerPiece;
	} // getGamePiece

	public void setGamePiece(char piece) {
		this.playerPiece = piece;
	} // getGamePiece

	@Override
	/*
	 * This method returns the name of the player instance
	 * 
	 * @params - void
	 * 
	 * @return - String - player name (non-Javadoc)
	 * 
	 * @see PlayerInterface#getName()
	 */
	public String getName() {
		return playerName;
	} // getName

	@Override
	/*
	 * This method is responsible for taking the inout from user, representing
	 * the next move of the player. (non-Javadoc)
	 * 
	 * @params - void
	 * 
	 * @return - int - column for next move.
	 * 
	 * @see PlayerInterface#nextMove()
	 */
	public int nextMove() {
		int moveCol = 0;
		try {
			moveCol = Integer.parseInt(myHandler.requestData().trim());
		} catch (NumberFormatException  e) {
			System.out.println("Connection to " + playerName + " lost");
			return -1;
		}
		// retun column for next move
		return moveCol;
	} // nextMove

	@Override
	public void view(String message) {
		myHandler.sendPacket(message);
	}

	@Override
	public ClientThread getHandler() {
		return myHandler;
	}
} // Player
