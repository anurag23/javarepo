/*
 * ClientThread.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * This class extends thread and can be used to creat threads responsible for
 * handling communication with a new online user. It asks the details of new
 * player and assigns it to the correct waiting list for Connect4Field game.
 * 
 * @author Anurag Malik, am3926
 */
public class ClientThread extends Thread {
	DatagramSocket serverSocket;
	GameServer server;
	InetAddress myIP;
	int myPort;
	boolean stop;
	Player myPlayer;
	volatile boolean dataRequired = false;
	private Object threadLock = new Object();
	private DatagramPacket newPacket;
	private Object threadLockServer = new Object();

	public ClientThread(DatagramPacket packet, DatagramSocket serverSocket,
			GameServer gameServer) {
		this.serverSocket = serverSocket;
		this.server = gameServer;
	}

	public void sendPacket(String message) {
		byte[] buffer = message.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, myIP,
				myPort);
		try {
			serverSocket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized void setup(DatagramPacket packet) {
		myIP = packet.getAddress();
		myPort = packet.getPort();
		String data = new String(packet.getData());
		String[] details = data.trim().split(" ");
		myPlayer = new Player(this, details[1]);

		// add the new player to the appropriate two player or four player
		// list on the server.
		switch (Integer.parseInt(details[0])) {
		case 1:
			server.addOnePlayerQueue(myPlayer);
			break;
		case 2:
			if (!server.canTwoPlayNext()) {
				sendPacket(details[1]
						+ "!! You'll have to wait for other players to join a two player game.");
			}
			server.addTwoPlayerQueue(myPlayer);
			break;
		case 4:
			if (!server.canFourPlayNext()) {
				sendPacket(details[1]
						+ "!! You'll have to wait for other players to join a four player game.");
			}
			server.addFourPlayerQueue(myPlayer);
			break;
		}
	}

	@Override
	/**
	 * Overridden method from Thread class, here it is responsible for communicating and 
	 * asking details from new user trying to conenct to the Connect4Field game server. 
	 * It then creates new Player instances.
	 * 
	 */
	public void run() {
		synchronized (threadLockServer) {
			try {
				threadLockServer.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		while (!stop) {
			try {
				synchronized (threadLock) {
					synchronized (threadLockServer) {
						if (dataRequired) {
							threadLockServer.wait();
							threadLock.notify();
						}
					}
					threadLock.wait();
				}
			} catch (Exception e) {

			}

		}
	}

	public void pushData(DatagramPacket packet) {
		synchronized (threadLockServer) {
			newPacket = packet;
			threadLockServer.notify();
		}
	}

	public String requestData() {
		synchronized (threadLock) {
			dataRequired = true;
			threadLock.notify();
			try {
				threadLock.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			dataRequired = false;
		}
		return new String(newPacket.getData());
	}

	public synchronized void wakeUp() {
		synchronized (threadLockServer) {
			threadLockServer.notify();
		}
	}
}
