/*
 * Bridge.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * This class represents a bridge, which allows a fixed number of vehicles to
 * cross through and also to maintain a balance of total weight that can be
 * present on the bridge at any time. Various Truck threads are allowed to pass
 * through the bridge only if there is a free bridge lock maintained by
 * associated BridgeServer thread.
 * 
 * Truck threads that can't pass due to number or weight limit are enqueued in a
 * waiting list of all trucks.
 * 
 * @author Anurag Malik, am3926
 * 
 */
class Bridge {
	String bridgeName;
	int truckLimit;
	static int weightLimit;
	static volatile int weightOnBridge;
	Truck lastInQueue, firstInQueue;
	Object[] locks;
	Object lastTruck;

	/**
	 * Bridge class constructor to initialize bridge instance
	 * 
	 * @param name
	 *            - bridge name
	 * @param maxTrucks
	 *            - maximum number of trucks allowed on bridge
	 * @param weight
	 *            - maximum weight limit allowed on bridge
	 */
	Bridge(String name, int maxTrucks, int weight) {
		bridgeName = name;
		truckLimit = maxTrucks;
		weightLimit = weight;
	}

	/**
	 * Open bridge instance, initialize BridgeServer threads.
	 * 
	 */
	public void open() {
		System.out.print(bridgeName + " opened with locks - ");
		locks = new Object[truckLimit];
		// create locks, and bridge servers
		for (int i = 0; i < truckLimit; i++) {
			locks[i] = new Object();
			System.out.print("LOCK_" + i + " ");
			// start bridge server instances
			new BridgeServer(this, locks[i], "LOCK_" + i).start();
		}
		System.out.println("\nOpen for vehicles.");
	}

	/**
	 * This method is responsible for fetching/ peeking the first truck in the
	 * waiting queue. If there is no truck waiting then threads go on wait.
	 * 
	 * @param peek
	 *            - boolean parameter to decide if it is a peek function only
	 * @return Truck instance, first in the queue
	 */
	public Truck getFirstInQueue(boolean peek) {
		synchronized (this) {
			// wait, if the waiting list in empty and there is no truck waiting
			while (firstInQueue == null) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			// if it is not a peek,
			// fetch first truck in queue and set first in queue = null
			Truck nextTruck = firstInQueue;
			if (!peek) {
				firstInQueue = null;
				this.notifyAll();
			}
			return nextTruck;
		}
	}

	/**
	 * This method in BridgeManager accepts the truck enqueue requests and
	 * provide each new truck with a reference of the last truck in waiting
	 * queue
	 * 
	 * @param nextTruck
	 *            - next truck to be enqueued
	 * @return - reference of last truck in queue
	 */
	public Truck bridgeEnqueue(Truck nextTruck) {
		synchronized (this) {
			if (firstInQueue == null) {
				// waiting list was empty, set current truck as first & last in
				// queue
				firstInQueue = nextTruck;
				this.notifyAll();
				lastInQueue = nextTruck;
				return null;
			}
			Truck prevTruck = lastInQueue;
			System.out.println(nextTruck.getTruckName() + " enqueued after "
					+ prevTruck.getTruckName());
			lastInQueue = nextTruck;
			return prevTruck;
		}
	}

	/**
	 * This method is used by truck threads to set themselves first in queue
	 * after all the truck prior to them in the waiting queue has already passed
	 * the bridge.
	 * 
	 * @param truck
	 *            - current truck thread reference
	 */
	public void moveToFirst(Truck truck) {
		synchronized (this) {
			// if first in queue is not null, that is there is a truck already
			// waiting in queue, then wait
			if (firstInQueue != null) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// set current truck as first in queue now.
			firstInQueue = truck;
			this.notifyAll();
		}
	}
}

/*
 * BridgeServer.java
 * 
 * Version: 1.0
 * 
 * Revisions: 1.0
 */
/**
 * This class is used to create bridge server threads, which are associated with
 * each lock avialable on the bridge. The trucks can cross bridge only when one
 * of these BridgeServer threads provide them a lock.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 */
class BridgeServer extends Thread {
	Object myLock;
	Bridge bridge;
	String name;
	static Object bridgeLock = new Object();

	/**
	 * Create a new bridge server instance, provide an exclusive lock to each
	 * instance.
	 * 
	 * @param bridge
	 *            - bridge instance
	 * @param lock
	 *            - lock instance
	 * @param name
	 *            - name of the lock
	 */
	public BridgeServer(Bridge bridge, Object lock, String name) {
		myLock = lock;
		this.bridge = bridge;
		this.name = name;
	}

	/**
	 * Overridden run method from Thread class, it is responsible for pricking
	 * up first truck in waiting queue and allocate a lock to it. The truck
	 * threads are waiting until one of these bridge server threads wake them up
	 * and provide a shared lock to cross the bridge. The BridgeServer thread is
	 * on wait until a truck crosses bridge and notifies it on the shared lock.
	 * 
	 */
	public void run() {
		while (true) {
			try {
				synchronized (myLock) {
					// get first truck in queue and wait if there is none
					Truck truck = getFirstInQueue();
					System.out.println("\n" + truck.getTruckName() + " / "
							+ truck.truckWeight
							+ "lbs allowed to enter bridge with " + name
							+ " lock.\nTotal weight on bridge is : "
							+ Bridge.weightOnBridge + "lbs\n");
					// allocate a lock to the truck instance and wait on lock
					truck.crossBridge(myLock);
					myLock.wait();
					System.out.println("\n" + truck.getTruckName()
							+ " crossed bridge.");
					synchronized (bridgeLock) {
						// reduce total current weight on bridge
						Bridge.weightOnBridge -= truck.truckWeight;
						System.out.println("Total weight on bridge is : "
								+ Bridge.weightOnBridge + "lbs");
						// unlock all other bridge server threads waiting on
						// bridge lock
						bridgeLock.notifyAll();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Synchronized access to bridge manager class, to peek/ fetch the next
	 * truck in waiting list.
	 * 
	 */
	private Truck getFirstInQueue() {
		// peek next first truck in queue
		Truck truck = bridge.getFirstInQueue(true);
		synchronized (bridgeLock) {
			// wait if the weight of current truck will go over the total
			// allowed limit for the bridge,
			// wait until on of the truck on bridge crosses and notifies.
			while (Bridge.weightOnBridge + truck.truckWeight > Bridge.weightLimit) {
				try {
					bridgeLock.wait();
					truck = bridge.getFirstInQueue(true);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// remove next truck from the queue and allow it to move on bridge
			truck = bridge.getFirstInQueue(false);
			// update total current weight on bridge
			Bridge.weightOnBridge += truck.truckWeight;
			return truck;
		}
	}
}
