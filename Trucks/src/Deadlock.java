
/*
 * Deadlock.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */


/**This class creates two threads and goes into a guaranteed deadlock
 * using synchronized and wait().
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 * 
*/
public class Deadlock extends Thread{
	
	// two static objects o1 and o2 on which synchronization is done
	//and locks are created for that block.
	static Object o1 = new Object();
    static Object o2 = new Object();
    
	public static void main(String[] args) {
		
		// Thread1 created and is in runnable state.
		Thread t1 = new Thread(){
			//run method for this thread.
			public void run(){
				
				//here lock is made on an object o1(shared resource) by thread t1.
				synchronized(o1){
					//lock is made on an object o2(shared resource) by the same thread.
					synchronized(o2){
						//1 is printed
						System.out.println("1");
						
						// a new thread is created inside the first thread and is in
						//runnable condition at the end of this block.
						new Thread(){
							//run method for this thread.
							public void run(){
								
								//this new thread takes the lock for object o2 as soon
								//as thread t1 releases the key to the lock for it and 
								//goes to wait state.
								synchronized(o2){
									//here when the new thread tries to take the lock for object o1
									//it is not allowed to take that lock as thread t1 already has the
									//lock for it and has not released the key to it.
									//Deadlock occurs here.
									synchronized(o1){ 
										//never gets printed.
										System.out.println("2");
									}
								}
							}
						}.start();
						
						try {
							//thread t1 goes to wait now and releases key to the lock for object o2.
							o2.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} 
			}
		};
		//thread t1 is made in runnable state
		t1.start();
 }
}