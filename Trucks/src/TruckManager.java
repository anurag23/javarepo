/*
 * TruckManager.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
import java.util.Random;

/**
 * This class simulates a scenario of multiple trucks crossing over a bridge.
 * TruckManager class is responsible for creating multiple truck threads.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class TruckManager extends Thread {
	Bridge bridge;
	int count;
	int index = 1;
	int maxWeight = 10000;
	int minWeight = 100;

	/**
	 * Constructor for initializing the truck manager class
	 * 
	 * @param bridge
	 *            - reference of the bridge, trucks will pass through
	 * @param truckCount
	 *            - maximum number of trucks to be released
	 */
	TruckManager(Bridge bridge, int truckCount) {
		this.bridge = bridge;
		this.count = truckCount;
	}

	/**
	 * Overriden method from the thread class, it is responsible for creating
	 * the required number of truck threads. Each truck is provided a random
	 * weight between 100- 10000 lbs. A delay of 200ms is introduced between
	 * creating each new truck thread.
	 * 
	 * @params - void
	 */
	public void run() {
		while (index <= count) {
			// create required number of truck threads
			new Truck(index++, (new Random().nextInt(maxWeight - minWeight))
					+ minWeight, bridge).start();
			try {
				sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

/**
 * This class represents a Truck. The class extends thread, such that each new
 * instance is a truck thread, which tries to cross a bridge.
 * 
 * @author Anurag Malik, am3926
 * @author Mayank Jain, mj2997
 */
class Truck extends Thread {
	String truckName;
	int truckId, truckWeight;
	Truck nextTruck;
	Bridge bridge;
	Object myBridgeLock, prevLock, myLock;
	Truck prevTruck;
	static int count;

	/**
	 * Constructor for initializing the truck thread instance.
	 * 
	 * @param id
	 *            - truck ID provided by a particular TruckManager
	 * @param weight
	 *            - weight to be loaded on the truck
	 * @param bridge
	 *            - reference of the bridge to be crossed
	 */
	Truck(int id, int weight, Bridge bridge) {
		truckName = "TRUCK_" + id;
		truckWeight = weight;
		truckId = id;
		nextTruck = null;
		// create a new lock for this truck
		myLock = new Object();
		this.bridge = bridge;
		System.out.println(truckName + " : reached bridge");
	}

	/**
	 * Ovverridden method from the Thread class, it is responsible for
	 * simulating a truck trying to cross through the bridge.
	 */
	public void run() {
		try {
			// as the bridge manager to enqueue the new truck in waiting queue,
			// and get reference of the truck prior to self
			prevTruck = bridge.bridgeEnqueue(this);
			// gain access of personal lock
			synchronized (myLock) {
				if (prevTruck != null) {
					// if there is a truck ahead, try gaining access to its lock
					prevLock = prevTruck.getLock();
					synchronized (prevLock) {
						// truck ahead in queue has moved onto bridge, now
						// current instance is first in waiting list
						bridge.moveToFirst(this);
					}
				}
				// got access of bridge,
				// release personal lock and grab bridge lock
				myLock.wait();
				synchronized (myBridgeLock) {
					// cross the bridge
					System.out.println(truckName + " : crossing");
					sleep(new Random().nextInt(3000));
					System.out.println(truckName + " : crossed");
					// release bridge lock
					myBridgeLock.notify();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method returns the name of the current truck instance
	 * 
	 * @return - String- name of truck instance
	 */
	public String getTruckName() {
		return truckName;
	}

	/**
	 * This method is used to receive notification from BridgeServer to know if
	 * current truck can move onto bridge and thus release personal lock for
	 * next truck to get ready.
	 * 
	 * @param myBridgeLock
	 *            - one of the locks available with BridgeManager
	 */
	public void crossBridge(Object myBridgeLock) {
		synchronized (myLock) {
			this.myBridgeLock = myBridgeLock;
			myLock.notify();
		}
	}

	/**
	 * this method returns the reference of the lock of current truck thread.
	 * This is used by next Truck in queue.
	 * 
	 * @return Lock reference
	 */
	public Object getLock() {
		return myLock;
	}

}
