/*
 * LondonCity.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */
/**
 * 
 * This class makes use of multithreaded Bridge and Truck Manager classes to
 * simulate a scenario of multiple trucks crossing through a bridge in a city.
 * 
 * @author Anurag Malik, am3926
 * 
 */
public class LondonCity {

	/**
	 * Create new bridge instance and start truck manager class to start
	 * creating multiple trucks to pass through the bridge
	 * 
	 * @param args
	 *            - command line parameters
	 */
	public static void main(String[] args) {
		// new bridge instance - provide name, maximum trucks allowed, maximum
		// weight allowed
		Bridge bridge = new Bridge("London Bridge", 4, 20000);
		// Start truck manager instance
		TruckManager truckMngr = new TruckManager(bridge, 15);
		bridge.open();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		truckMngr.start();

	}

}
