/*
 * SpeedServer.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class acts as the server for testing the throughput through TCP and UDP
 * connections. The type of connection to be tested is passed through the
 * commandline arguments.
 * 
 * @author Anurag Malik, am3926
 */
public class SpeedServer extends Thread {
	static String threadName;

	/*
	 * The program execution starts from this method, and implements a thread to
	 * test for TCP or UDP connection based upon the input commandline
	 * parameter.
	 */
	public static void main(String[] args) throws Exception {
		// create new server testing thread
		System.out.println("Server Running at IP :" + InetAddress.getByName("localhost"));
		SpeedServer server = new SpeedServer(args[1]);
		server.start();
	}

	SpeedServer(String name) {
		threadName = name;
	}

	/**
	 * Overridden run method from thread class, that
	 */
	public void run() {
		// checked what type of connection is to be checked
		if (threadName.equals("TCP")) {
			System.out.println("Server TCP throughput test.");
			testTCP();
		} else {
			System.out.println("Server UDP throughput test.");
			testUDP();
		}
	} // run

	/**
	 * this method is used to test TCP throughput, it continuously keeps reading
	 * data from a client. the data is read in chunks of 64Kbs from the server
	 * socket input stream.
	 */
	public void testTCP() {
		try {
			// create new server socket
			ServerSocket serverSocket = new ServerSocket(4040);
			Socket socket = serverSocket.accept();

			// get inputsteam on server socket
			InputStream input = socket.getInputStream();
			long total = 0;

			// read 64Kb of data chunk and start timer.
			byte[] bytes = new byte[64 * 1024];
			long start = System.currentTimeMillis();
			for (int i = 1;; i++) {
				int data = input.read(bytes);
				if (data < 0)
					break;
				total += data;
				// after every 200 miliseconds output results
				if (i % 200000 == 0) {
					long timeSpent = System.currentTimeMillis() - start;
					System.out.println("Data read : " + total / 1024
							+ " Kbs, speed: " + (total / timeSpent) / 1000
							+ " MB/s");
				}
			}
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// testTCP

	/**
	 * this method is used to test UDP throughput, it continuously keeps reading
	 * data from a client. the data is read in chunks of almost 64Kbs from the server
	 * socket input stream.
	 */
	public void testUDP() {
		try {
			// create new data gram socket to listen from server port
			DatagramSocket socket = new DatagramSocket(4040);
			long total = 0;

			DatagramPacket packet;
			byte[] data;
			// start timer
			long start = System.currentTimeMillis();
			for (int i = 1;; i++) {
				data = new byte[64 * 1000];
				packet = new DatagramPacket(data, data.length);
				socket.receive(packet);

				total += data.length;
				// after every 200 miliSeconds, print out the results
				if (i % 200000 == 0) {
					long timeSpent = System.currentTimeMillis() - start;
					System.out.println("Data read : " + total / 1024
							+ " Kbs, speed: " + (total / timeSpent) / 1000
							+ " MB/s");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	} // testUDP
} // SpeedServer