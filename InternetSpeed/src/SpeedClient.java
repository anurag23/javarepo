/*
 * SpeedClient.java
 *
 * Version:
 *     1.0
 *
 * Revisions:
 *     1.0
 */

import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * This class acts as the client for testing TCP and UDP throughput at a
 * designated server.
 * 
 * @author Anurag Malik, am3926
 */
public class SpeedClient {
	static String serverIP;
	static int serverPort = 4040;

	/**
	 * Take server IP address as input and then decide wether to start TCP/ UDP
	 * throughout test based upon the input commandline parameters
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		SpeedClient server = new SpeedClient();
		Scanner scr = new Scanner(System.in);
		serverIP = scr.nextLine();

		// match argument and decide which test to perform
		if (args[1].toUpperCase().equals("TCP")) {
			System.out.println("Testing TCP throughput for Server :" + serverIP
					+ ", Port :" + serverPort);
			server.testTCP();
		} else {
			System.out.println("Testing UDP throughput for Server :" + serverIP
					+ ", Port :" + serverPort);
			server.testUDP();
		}
	}

	/**
	 * This method is used to pack data bytes in chunk of 64Kbs and send over to
	 * TCP conenction to a server.
	 */
	public void testTCP() {
		try {
			Socket socket = new Socket(serverIP, serverPort);
			System.out.println("Starting to send data to Server");
			OutputStream out = socket.getOutputStream();

			// pack data and send to output stream over socket
			byte[] data = new byte[64 * 1024];
			while (true)
				out.write(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to pack data bytes in chunk of almost 64Kbs data and
	 * send over to UDP connection to a dedicated server.
	 */
	public void testUDP() {
		try {
			// create datagram socket
			DatagramSocket socket = new DatagramSocket();
			byte[] data = new byte[64 * 1000];
			// pack new datagram socket
			DatagramPacket packet = new DatagramPacket(data, data.length,
					InetAddress.getByName(serverIP), serverPort);
			System.out.println("Starting to send data to Server");
			while (true)
				// send packet over the datagram socket
				socket.send(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
